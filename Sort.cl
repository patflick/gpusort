

void compare(__local uint* localBlock, uint i, uint j) {
	// ASSERT i < j
	if (localBlock[i] > localBlock[j]) {
		// swap
		uint tmp = localBlock[i];
		localBlock[i] = localBlock[j];
		localBlock[j] = tmp;
	}
}


void OddEvenMerge(__local uint* localBlock, uint n) {
	uint lid = get_local_id(0);
	
	uint m = n;
	uint r = m/2;
	uint i = lid % n;
	if (i < r)
		compare(localBlock, lid, lid+r);
		
	// barrier
	barrier(CLK_LOCAL_MEM_FENCE);
	
	for (m = n/2; m >=2; m /= 2) {
		r = m / 2;
		if ((i / r ) % 2 == 1 && (i+r<n)) {
			compare(localBlock, lid, lid+r);
		}
		// barrier
		barrier(CLK_LOCAL_MEM_FENCE);
	}
}




//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__kernel void OddEvenMergesort(__global uint* array, uint size, __local uint* localBlock) 
{
	uint lid = get_local_id(0);
	uint gid = get_global_id(0);
	uint localSize = get_local_size(0);
	
	if (gid >= size) return;
	
	localBlock[lid] = array[gid];
	
	// barrier
	barrier(CLK_LOCAL_MEM_FENCE);
	
	// local sort
	for (uint n = 2; n <= localSize; n*=2) {
		OddEvenMerge(localBlock, n);
		// barrier
		barrier(CLK_LOCAL_MEM_FENCE);
	}
	
	// set result into array
	array[gid] = localBlock[lid];
}


uint local_upper_bound(__local uint* localBlock, uint value, uint size) {
	uint len = size;
	uint middle;
	uint half;
	uint first;
	uint midVal;
	first = 0;
	while (len > 0) {
		half = len >> 1;
		middle = first + half;
		midVal = localBlock[middle];
		if (midVal <= value) {
			first = middle+1;
			len = len - half - 1;
		} else {
			len = half;
		}
	}
	return first;
}

uint global_upper_bound(__global uint* array, uint beginIndex, uint endIndex, uint value) {
	uint len = endIndex - beginIndex;
	uint middle;
	uint half;
	uint first;
	uint midVal;
	first = beginIndex;
	while (len > 0) {
		half = len >> 1;
		middle = first + half;
		midVal = array[middle];
		if (midVal <= value) {
			first = middle+1;
			len = len - half - 1;
		} else {
			len = half;
		}
	}
	return first;
}

uint local_lower_bound(__local uint* localBlock, uint value, uint size) {
	uint len = size;
	uint middle;
	uint half;
	uint first;
	uint midVal;
	first = 0;
	while (len > 0) {
		half = len >> 1;
		middle = first + half;
		midVal = localBlock[middle];
		if (midVal < value) {
			first = middle+1;
			len = len - half - 1;
		} else {
			len = half;
		}
	}
	return first;
}

uint global_lower_bound(__global uint* array, uint beginIndex, uint endIndex, uint value) {
	uint len = endIndex - beginIndex;
	uint middle;
	uint half;
	uint first;
	uint midVal;
	first = beginIndex;
	while (len > 0) {
		half = len >> 1;
		middle = first + half;
		midVal = array[middle];
		if (midVal < value) {
			first = middle+1;
			len = len - half - 1;
		} else {
			len = half;
		}
	}
	return first;
}


__kernel void LocalMerge(__global uint* array, uint size, __local uint* localBlockA, __local uint* localBlockB, __local uint* localResult)
{
	uint lid = get_local_id(0);
	uint gid = get_global_id(0);
	uint localSize = get_local_size(0);
	uint gOffset = 2*localSize*get_group_id(0);
	
	if (gid >= size/2) return;
	
	localBlockA[lid] = array[gOffset+lid];
	localBlockB[lid] = array[gOffset+localSize+lid];
	
	// barrier
	barrier(CLK_LOCAL_MEM_FENCE);
	
	uint i = lid;
	uint Ai = localBlockA[i];
	uint Bi = localBlockB[i];
	
	uint rankAiB = local_lower_bound(localBlockB, Ai, localSize);
	uint rankBiA = local_upper_bound(localBlockA, Bi, localSize);
	
	localResult[i + rankAiB] = Ai;
	localResult[i + rankBiA] = Bi;
	
	// barrier
	barrier(CLK_LOCAL_MEM_FENCE);
	
	array[gOffset+lid] = localResult[lid];
	array[gOffset+localSize+lid] = localResult[localSize+lid];
}

__kernel void LocalSplitterMerge(__global uint* inArray, global uint* outArray, uint size, __local uint* localBlockA, __local uint* localBlockB, __local uint* localResult)
{
	uint lid = get_local_id(0);
	uint gid = get_global_id(0);
	uint localSize = get_local_size(0);
	uint gOffset = 2*localSize*get_group_id(0);
	
	if (gid >= size/2) return;
	
	localBlockA[lid] = inArray[gOffset+lid];
	localBlockB[lid] = inArray[gOffset+localSize+lid];
	
	// barrier
	barrier(CLK_LOCAL_MEM_FENCE);
	
	uint i = lid;
	uint Ai = localBlockA[i];
	uint Bi = localBlockB[i];
	
	uint rankAiB = local_lower_bound(localBlockB, Ai, localSize);
	uint rankBiA = local_upper_bound(localBlockA, Bi, localSize);
	
	localResult[i + rankAiB] = gOffset + i;
	localResult[i + rankBiA] = gOffset + localSize + i;
	
	// barrier
	barrier(CLK_LOCAL_MEM_FENCE);
	
	outArray[gOffset+lid] = localResult[lid];
	outArray[gOffset+localSize+lid] = localResult[localSize+lid];
	//outArray[gOffset+lid] = 0;
	//outArray[gOffset+localSize+lid] = 0;
}



void SplitterLocalMerge(__global uint* inArray, __global uint* outArray, uint beginA, uint endA, uint beginB, uint endB, uint seqOffset, uint seqSize, __local uint* localBlockA, __local uint* localBlockB, __local uint* localResult, bool outputIndex)
{
	uint lid = get_local_id(0);
	uint gid = get_global_id(0);
	uint localSize = get_local_size(0);
	
	uint outputOffset = seqOffset + (beginA - seqOffset) + (beginB - seqOffset - seqSize);
	
	uint sizeA = endA - beginA;
	uint sizeB = endB - beginB;
	if (lid < sizeA)
		localBlockA[lid] = inArray[beginA+lid];
	if (lid < sizeB)
		localBlockB[lid] = inArray[beginB+lid];
	
	// barrier
	barrier(CLK_LOCAL_MEM_FENCE);
	
	uint i = lid;
	
	
	uint Ai, Bi, rankBiA, rankAiB;
	// TODO sonderfaelle wenn eine sequenz laenger ist etc
	if (i < sizeA) {
		Ai = localBlockA[i];
		rankAiB = local_lower_bound(localBlockB, Ai, sizeB);
	}
	if (i < sizeB) {
		Bi = localBlockB[i];
		rankBiA = local_upper_bound(localBlockA, Bi, sizeA);
	}
	
	if (outputIndex) {
		if (i < sizeA)
			localResult[i + rankAiB] = (beginA)+lid;
		if (i < sizeB)
			localResult[i + rankBiA] = (beginB)+lid;
	} else {
		if (i < sizeA)
			localResult[i + rankAiB] = Ai;
		if (i < sizeB)
			localResult[i + rankBiA] = Bi;
	}
	
	// barrier
	barrier(CLK_LOCAL_MEM_FENCE);
	
	if (lid < sizeA)
		outArray[outputOffset+lid] = localResult[lid];
	if (lid < sizeB)
		outArray[outputOffset+sizeA+lid] = localResult[sizeA+lid];
}

/*
 * Sample every t element as splitter
 */
__kernel void SampleSplitterAB(__global uint* array, __global uint* splitterAB, uint numSplitter, uint t) {
	//.gid is the index for splittersAB
	uint gid = get_global_id(0);
	
	if (gid >= numSplitter) return;
	
	//uint localSize = get_local_size(0);
	
	// sample every 'localSize' element
	splitterAB[gid] = array[gid*t + t-1];
}

__kernel void SearchSplitters(__global uint* array, __global uint* splittersAB, __global uint* mergedSplitters, __global uint* splitters, uint numSplitters, uint splittersSeqSize, uint t) {
	// gid is the index for the splitters
	uint gid = get_global_id(0);
	uint localSize = get_local_size(0);
	
	if (gid >= numSplitters) return;
	
	// index in splitters only for 2 sequences
	uint splittersSeqId = gid % (2*splittersSeqSize);
	uint splitterSeqOffset = gid - splittersSeqId;
	uint globalOffset = splitterSeqOffset*t;
	
	uint splitterABIndex = mergedSplitters[gid];
	
	
	uint sA, sB, rank_sA, rank_sB, Ai, Bi;
	
	// our element is from the B sequence
	if (splitterABIndex - splitterSeqOffset >= splittersSeqSize) {
		rank_sB = splitterABIndex - splitterSeqOffset - splittersSeqSize;
		sB = splittersAB[splitterABIndex];
		// index of the splitting element in input Array
		Bi = t*splitterABIndex + t;
		
		// rank(s,A) = rank(s,S) - rank(s,B)
		rank_sA = splittersSeqId - rank_sB;
		
		// search
		// Ai = search in global array between (rank_sA-1 (exlk), rank_sA(inkl))*localSize + some global offset
		if (rank_sA < splittersSeqSize)
			Ai = global_lower_bound(array, rank_sA*t + globalOffset, (rank_sA+1)*t + globalOffset, sB);
		else 
			Ai = splittersSeqSize*t + globalOffset;
	} else {
		rank_sA = splitterABIndex - splitterSeqOffset;
		sA = splittersAB[splitterABIndex];
		Ai = t*splitterABIndex + t;
		
		// rank(s,B) = rank(s,S) - rank(s,A)
		rank_sB = splittersSeqId - rank_sA;
		
		// search
		// Ai = search in global array between (rank_sA-1 (exlk), rank_sA(inkl))*localSize + some global offset
		if (rank_sB < splittersSeqSize)
			Bi = global_upper_bound(array, (splittersSeqSize + rank_sB)*t + globalOffset, (splittersSeqSize + rank_sB+1)*t + globalOffset, sA);
		else 
			Bi = 2*splittersSeqSize*t + globalOffset;
	}
	splitters[2*gid] = Ai;
	splitters[2*gid + 1] = Bi;
}

__kernel void SplitterMerge(__global uint* inArray, __global uint* outArray, uint size, __global uint* splitters, uint seqSize,  __local uint* localBlockA, __local uint* localBlockB, __local uint* localResult, uint outputIndex) {
	// one thread for 2 elements
	uint gid = get_global_id(0);
	uint lid = get_local_id(0);
	
	uint localSize = get_local_size(0);
	uint localSplitterIndex = (gid % (2*seqSize)) / localSize;
	uint splitterIndex = gid / localSize;
	
	uint seqOffset = gid - (gid % (2*seqSize));
	
	
	uint beginA, endA, beginB, endB;
	if (localSplitterIndex == 0) {
		beginA = seqOffset;
		beginB = seqOffset+seqSize;
	} else {
		beginA = splitters[2*splitterIndex-2];
		beginB = splitters[2*splitterIndex-1];
	}
	endA = splitters[2*splitterIndex];
	endB = splitters[2*splitterIndex+1];

	bool outInd = outputIndex > 0;
	SplitterLocalMerge(inArray, outArray, beginA, endA, beginB, endB, seqOffset, seqSize, localBlockA, localBlockB, localResult, outInd);
}


__kernel void SlowBinaryMerge(__global uint* inArray, __global uint* outArray, uint size, uint seqSize) {
	uint GID = get_global_id(0);
	uint numSeq = size/seqSize;
	
	// one thread for 2 sequences
	if (GID >= numSeq/2) return;
	uint seqOffset = GID * (2*seqSize);
	
	uint leftIndex = seqOffset;
	uint rightIndex = seqOffset + seqSize;
	
	uint outputOffset = seqOffset;
	uint curOutput = outputOffset;
	
	uint curLeft = leftIndex;
	uint curRight = rightIndex;
	
	for (uint i = 0; i < 2*seqSize; ++i) {
		if (curRight >= rightIndex+seqSize || (curLeft < rightIndex && inArray[curLeft] < inArray[curRight])) {
			outArray[curOutput] = inArray[curLeft];
			curOutput++;
			curLeft++;
		} else {
			outArray[curOutput] = inArray[curRight];
			curOutput++;
			curRight++;
		}
	}
}
