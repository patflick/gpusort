// inclusive prefix sum
__kernel void Scan_Naive(const __global uint* inArray, __global uint* outArray, uint N, uint offset) 
{

	uint right_index = get_global_id(0);
	int left_index = right_index - offset;
	
	if (right_index < N) {
		outArray[right_index] = inArray[right_index];
		if (left_index >= 0) {
			outArray[right_index] += inArray[left_index];
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Some usefull defines
#define NUM_BANKS			32
#define NUM_BANKS_LOG		5
#define SIMD_GROUP_SIZE		32

// Bank conflicts
#define AVOID_BANK_CONFLICTS
#ifdef AVOID_BANK_CONFLICTS
	// define your conflict free macro here
	#define OFFSET(A) ((A)+((A)>>NUM_BANKS_LOG)) 
#else
	#define OFFSET(A) (A)
#endif

__kernel void Scan_WorkEfficient(__global uint* array, __global uint* higherLevelArray, __local uint* localBlock) 
{

	uint LID = get_local_id(0);
	uint LS = get_local_size(0);
	uint GID = get_global_id(0);
	uint Goffset = get_group_id(0)*LS*2;
	
	// copy data from global to local array
	// coalescing access by first copying the first half then the second half
	localBlock[OFFSET(LID)] = array[Goffset+LID];
	localBlock[OFFSET(LS+LID)] = array[Goffset+LS+LID];
	
	// barrier waiting for local mem
	barrier(CLK_LOCAL_MEM_FENCE);

	/*
	// first step: Up-sweep
	int threadmapping = (LID*2)+1;
	for (int stride = 1; stride < LS*2; stride *= 2) {
		barrier(CLK_LOCAL_MEM_FENCE);
		if ((threadmapping+1) % (stride*2) == 0) {
			localBlock[OFFSET(threadmapping)] = localBlock[OFFSET(threadmapping-stride)] + localBlock[OFFSET(threadmapping)];
		}
	} 
	*/
	
	
	// more efficient version
	uint threads = LS;
	int stride = 1;
	for ( ; threads > SIMD_GROUP_SIZE; threads >>= 1) {
		if (LID < threads) {
			localBlock[OFFSET(2*LS-1-2*stride*LID)] += localBlock[OFFSET(2*LS-1-stride*(2*LID+1))];
		}
		
		barrier(CLK_LOCAL_MEM_FENCE);
		
		stride <<= 1;
	}
	
	
	// Barrier avoidance:
	if (LID < SIMD_GROUP_SIZE) {
		// first step: Up-sweep
		// last 32 threads without barrier (warp)
		for ( ; threads > 0; threads >>= 1) {	
			if (LID < threads) {
				localBlock[OFFSET(2*LS-1-2*stride*LID)] += localBlock[OFFSET(2*LS-1-stride*(2*LID+1))];
			}
			stride <<= 1;
		}
	
		
		if (LID == 0) {
			// last thread: write into higher level buffer
			higherLevelArray[get_group_id(0)] = localBlock[OFFSET(2*LS-1)];
			localBlock[OFFSET(2*LS-1)] = 0;
		}

		// second step: Down-sweep
		threads = 1;
		stride = LS;
		// barrier avoidance, the first 32 threads are a warp
		for ( ; threads <= SIMD_GROUP_SIZE && threads <= LS; threads <<= 1) {
			if (LID < threads) {
				// first read then write
				uint left_child_value = localBlock[OFFSET(2*LS-1-2*stride*LID-stride)];
				uint right_child_value = localBlock[OFFSET(2*LS-1-2*stride*LID)];
				localBlock[OFFSET(2*LS-1-2*stride*LID-stride)] = right_child_value;
				localBlock[OFFSET(2*LS-1-2*stride*LID)] = left_child_value + right_child_value;
			}
			stride >>= 1;
		}
	}
	else
	{
		threads <<= 1;
		stride >>= 1;
	}
	
	for ( ; threads <= LS; threads <<= 1) {
		barrier(CLK_LOCAL_MEM_FENCE);
		if (LID < threads) {
			// first read then write
			uint left_child_value = localBlock[OFFSET(2*LS-1-2*stride*LID-stride)];
			uint right_child_value = localBlock[OFFSET(2*LS-1-2*stride*LID)];
			localBlock[OFFSET(2*LS-1-2*stride*LID-stride)] = right_child_value;
			localBlock[OFFSET(2*LS-1-2*stride*LID)] = left_child_value + right_child_value;
		}
		stride >>= 1;
	}
	
	/*
	for (int stride = LS; stride > 0; stride /= 2) {
		barrier(CLK_LOCAL_MEM_FENCE);
		if ((threadmapping+1) % (stride*2) == 0) {
			// first read then write
			uint left_child_value = localBlock[OFFSET(threadmapping-stride)];
			uint right_child_value = localBlock[OFFSET(threadmapping)];
			localBlock[OFFSET(threadmapping-stride)] = right_child_value;
			localBlock[OFFSET(threadmapping)] = left_child_value + right_child_value;
		}
	}
	
	*/


	// barrier
	barrier(CLK_LOCAL_MEM_FENCE);
	
	// add local results to global array
	array[Goffset+LID] += localBlock[OFFSET(LID)];
	array[Goffset+LS+LID] += localBlock[OFFSET(LS+LID)];
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Kernel that should add the group PPS to the local PPS (Figure 14)
__kernel void Scan_WorkEfficientAdd(__global uint* higherLevelArray, __global uint* array, __local uint* localBlock) 
{
  	uint LID = get_local_id(0);
	uint GID = get_global_id(0);
	uint LS = get_local_size(0);
	uint Goffset = get_group_id(0)*LS*2;
	
	// every thread writes two!
	if (get_group_id(0) > 0) {
		array[Goffset+LID] += higherLevelArray[get_group_id(0)-1];
		array[Goffset+LS+LID] += higherLevelArray[get_group_id(0)-1];
	}

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

__kernel void GetBitFlags(const __global uint* inArray, __global uint* flags, uint N, uint bit, uint invert) {
	uint GID = get_global_id(0);
	
	if (GID >= N) return;
	
	uint value = inArray[GID];
	
	uint mask = 1 << bit;
	
	uint bitValue;
	if ((mask & value) == 0) {
		bitValue = 0;
	} else {
		bitValue = 1;
	}
	
	if (invert > 0) {
		bitValue = 1 - bitValue;
	}
	
	flags[GID] = bitValue;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

__kernel void AddLastElement(const __global uint* lastElementArray, __global uint* outArray, uint N) {
	uint GID = get_global_id(0);
	if (GID >= N) return;
	
	uint elementToAdd = lastElementArray[N-1];
	outArray[GID] += elementToAdd;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

__kernel void PermutePS(__global uint* inArray, __global uint* outArray, __global uint* zeroPS, __global uint* onePS, uint N, uint bit) {
	uint GID = get_global_id(0);
	if (GID >= N) return;
	
	uint value = inArray[GID];
	uint mask = 1 << bit;
	uint outputIndex;
	if ((mask & value) == 0) {
		outputIndex = zeroPS[GID]-1;
	} else {
		outputIndex = onePS[GID]-1;
	}
	outArray[outputIndex] = value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define OFFSET(A) (A)
// taken in part from Scan.cl, no barrier avoidance yet
void localPrefixSum(__local uint* localBlock) {
	uint LID = get_local_id(0);
	uint LS = get_local_size(0);
	
	LS = LS / 2;
	
	uint threads = LS;
	int stride = 1;
	for ( ; threads > 0; threads >>= 1) {
		if (LID < threads) {
			localBlock[OFFSET(2*LS-1-2*stride*LID)] += localBlock[OFFSET(2*LS-1-stride*(2*LID+1))];
		}
		
		barrier(CLK_LOCAL_MEM_FENCE);
		
		stride <<= 1;
	}
	
	if (LID == 0) {
		// last thread: write into higher level buffer
		localBlock[OFFSET(2*LS-1)] = 0;
	}
	
	threads = 1;
	stride = LS;
	for ( ; threads <= LS; threads <<= 1) {
		barrier(CLK_LOCAL_MEM_FENCE);
		if (LID < threads) {
			// first read then write
			uint left_child_value = localBlock[OFFSET(2*LS-1-2*stride*LID-stride)];
			uint right_child_value = localBlock[OFFSET(2*LS-1-2*stride*LID)];
			localBlock[OFFSET(2*LS-1-2*stride*LID-stride)] = right_child_value;
			localBlock[OFFSET(2*LS-1-2*stride*LID)] = left_child_value + right_child_value;
		}
		stride >>= 1;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void localGetFlags(__local uint* localInput, __local uint* flagsOut, uint bit, uint invert) {
	uint LID = get_local_id(0);
	
	uint value = localInput[LID];
	
	uint mask = 1 << bit;
	
	uint bitValue;
	if ((mask & value) == 0) {
		bitValue = 0;
	} else {
		bitValue = 1;
	}
	
	if (invert > 0) {
		bitValue = 1 - bitValue;
	}
	
	flagsOut[LID] = bitValue;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



__kernel void LocalSplitRadixSort(__global uint* array, __global uint* localHistrograms, __local uint* localBlock1, __local uint* localBlock2, __local uint* localZeroPS, __local uint* localOnePS, uint N, uint startbit, uint b) {
	uint GID = get_global_id(0);
	uint LID = get_local_id(0);
	
	//if (GID >= N) return;
	
	uint lastLocalIndex = get_local_size(0)-1;
	if (get_group_id(0) == (get_num_groups(0)-1)) {
		lastLocalIndex = (N-1) % get_local_size(0);
	}
	
	__local uint* pingArray = localBlock1;
	__local uint* pongArray = localBlock2;
	
	if (LID <= lastLocalIndex)
		pingArray[LID] = array[GID];
	
	barrier(CLK_LOCAL_MEM_FENCE);

	
	for (uint bit = startbit; bit < (startbit+b); bit++) {
		// get flags 0 1 0 1 1 ...
		localGetFlags(pingArray, localZeroPS, bit, 1);
		localGetFlags(pingArray, localOnePS, bit, 0);
		
		barrier(CLK_LOCAL_MEM_FENCE);
		

		
		uint lastZeroElement = localZeroPS[lastLocalIndex];
		
		// calc prefix sum (exclusive sum)
		localPrefixSum(localZeroPS);
		localPrefixSum(localOnePS);
		
		barrier(CLK_LOCAL_MEM_FENCE);
		
		// add total sum of zeros to all elements in onePS
		localOnePS[LID] += (localZeroPS[lastLocalIndex] + lastZeroElement);
		
		barrier(CLK_LOCAL_MEM_FENCE);
		
		// permutate
		
		uint value = pingArray[LID];
		uint mask = 1 << bit;
		uint outputIndex;
		if ((mask & value) == 0) {
			outputIndex = localZeroPS[LID];
		} else {
			outputIndex = localOnePS[LID];
		}
		
		barrier(CLK_LOCAL_MEM_FENCE);
		if (LID <= lastLocalIndex)
			pongArray[outputIndex] = value;
		
		barrier(CLK_LOCAL_MEM_FENCE);
		
		// switch Ping and Pong
		__local uint* tmp = pingArray;
		pingArray = pongArray;
		pongArray = tmp;
	}
	
	// create histrogram ASSERT ( 2^b <= localsize)
	uint numBuckets = 1 << b;
	// we dont need the localZerosPS array anymore
	__local uint* localBuckets = localZeroPS; 
	if (LID < numBuckets) {
		localBuckets[LID] = 0;
	}
	
	uint bMask = ((1 << b) - 1) << startbit;
	
	if (LID < lastLocalIndex) {
		if ((pingArray[LID] & bMask) < (pingArray[LID+1] & bMask)) {
			uint bucketIndex = (pingArray[LID] & bMask) >> startbit;
			uint nextBucketIndex = (pingArray[LID+1] & bMask) >> startbit;
			for (uint i = bucketIndex; i < nextBucketIndex; i++) {
				localBuckets[i] = LID+1;
			}
		}
	} else if (LID == lastLocalIndex) {
		uint bucketIndex = (pingArray[LID] & bMask) >> startbit;
		for (uint i = bucketIndex; i < numBuckets; i++) {
			localBuckets[i] = LID+1;
		}
	}
	
	barrier(CLK_LOCAL_MEM_FENCE);
	
	// write out histograms
	if (LID < numBuckets) {
		uint histIndex = LID*get_num_groups(0) + get_group_id(0);
		
		if (LID > 0)
			localHistrograms[histIndex] = localBuckets[LID] - localBuckets[LID-1];
		else
			localHistrograms[histIndex] = localBuckets[LID];
	}
	
	// write out sort results
	if (LID <= lastLocalIndex)
		array[GID] = pingArray[LID];
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

__kernel void HistrogramShuffle(__global uint* inArray, __global uint* outArray, __global uint* localHistrogramsPS, __local uint* localBuckets, uint N, uint startbit, uint b) {
	uint GID = get_global_id(0);
	uint LID = get_local_id(0);
	uint bMask = ((1 << b) - 1) << startbit;
	uint numBuckets = (1 << b);
	
	if (GID >= N) return;
	
	uint lastLocalIndex = get_local_size(0)-1;
	if (get_group_id(0) == (get_num_groups(0)-1)) {
		lastLocalIndex = (N-1) % get_local_size(0);
	}
	
	if (LID < numBuckets) {
		localBuckets[LID] = 0;
	}
	
	
	uint value = inArray[GID];
	uint bucketIndex = (value & bMask) >> startbit;

	if (LID < lastLocalIndex) {
		uint nextValue = inArray[GID+1];
		if ((value & bMask) < (nextValue & bMask)) {
			uint nextBucketIndex = (nextValue & bMask) >> startbit;
			for (uint i = bucketIndex; i < nextBucketIndex; i++) {
				localBuckets[i] = LID+1;
			}
		}
	} else if (LID == lastLocalIndex) {
		localBuckets[bucketIndex] = LID+1;
	}
	
	barrier(CLK_LOCAL_MEM_FENCE);
	
	
	
	uint histIndex = bucketIndex*get_num_groups(0) + get_group_id(0);
	
	uint localOffset = LID;
	if (bucketIndex > 0) {
		localOffset = LID - localBuckets[bucketIndex-1];
	}
	
	uint globalOffset = 0;
	if (histIndex > 0) {
		globalOffset = localHistrogramsPS[histIndex-1];
	}
	
	outArray[globalOffset + localOffset] = value;
}

