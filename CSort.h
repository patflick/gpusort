/*
*/

#ifndef CSort_H
#define CSort_H

#include "IAssignment.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class CSort
	:public IAssignment
{
protected:
	//NOTE: we have two memory address spaces, so we mark pointers with a prefix
	//to avoid confusions: 'h' - host, 'd' - device

	unsigned int		m_N;

	// input data
	unsigned int		*m_hInput;
	// results
	unsigned int		*m_resultCPU;
	unsigned int		*m_resultGPU;

	cl_mem				m_dArray;
	
	cl_mem				m_dSplitterAB;

	//OpenCL program and kernels
	cl_program			m_Program;
	cl_kernel			m_OddEvenMergesortKernel;
	cl_kernel			m_LocalSplitterMergeKernel;
	cl_kernel			m_LocalMergeKernel;
	cl_kernel			m_SearchSplittersKernel;
	cl_kernel			m_SampleSplitterAB;
	cl_kernel			m_SplitterMergeKernel;
	cl_kernel			m_SlowBinaryMergeKernel;
	
	void Sort_OddEvenMergesort(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3]);
	void Sort_LocalMerge(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3]);
	void Sort_SampleSplitterAB(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3], size_t t);
	
	void Sort_LocalSplitterMerge(cl_context Context, cl_command_queue CommandQueue, cl_mem& splitterAB, cl_mem& mergedSplitter, size_t numSplitter, size_t splitterSeqSize);
	
	void Sort_MergeSort(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3]);
	void splitterMerge(cl_context Context, cl_command_queue CommandQueue, size_t localWorkSize, cl_mem& inArray, cl_mem& outArray, unsigned int size, cl_mem& splitters, unsigned int seqSize, bool indexOutput);
	void getSplitters(cl_context Context, cl_command_queue CommandQueue, cl_mem& dArray, cl_mem& splitters, unsigned int numSequences, unsigned int seqLength, size_t t, size_t localWorkSize);
	void calcSplittersAB(cl_context Context, cl_command_queue CommandQueue, size_t localWorkSize, unsigned int numSplitter, size_t t, cl_mem& dArray, cl_mem& dSplitterAB);
	void mergeSplittersAB(cl_context Context, cl_command_queue CommandQueue, size_t localWorkSize, unsigned int numSplitter, unsigned int splitterSeqSize, cl_mem& dSplitterAB, cl_mem& dMergedSplitters);
	void searchSplitters(cl_context Context, cl_command_queue CommandQueue, size_t localWorkSize, cl_mem& dArray, cl_mem& dSplitterAB, cl_mem& dMergedSplitters, cl_mem& splitters, unsigned int numSplitters, unsigned int splitterSeqSize, size_t t);
	
	// slow binary merge:
	void Sort_SlowMergeSort(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3]);
	void slowBinaryMerge(cl_context Context, cl_command_queue CommandQueue, size_t localWorkSize, cl_mem& inArray, cl_mem& outArray, unsigned int size, unsigned int seqSize);
	
	void ValidateTask(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3], unsigned int task);
	

public:
	CSort(unsigned int N);

	virtual ~CSort();

	void TestPerformance(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3], unsigned int task);
	
	virtual bool InitResources(cl_device_id Device, cl_context Context);
	
	virtual void ReleaseResources();

	virtual void ComputeGPU(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3]);

	virtual void ComputeCPU();

	virtual bool ValidateResults();
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#endif
