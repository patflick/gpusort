#include "CScan.h"
#include "CTimer.h"
#include "Common.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CReduction

CScan::CScan(unsigned int N, size_t LocalWorkSize[3]) 
	: m_N(N), m_hArray(NULL), m_hResultCPU(NULL), m_hResultGPU(NULL), 
	m_dPingArray(NULL), m_dPongArray(NULL),
	m_Program(NULL), 
	m_ScanNaiveKernel(NULL), m_ScanWorkEfficientKernel(NULL), m_ScanWorkEfficientAddKernel(NULL)
{
	memcpy(m_LocalWorkSize, LocalWorkSize, 3 * sizeof(size_t));
	// compute the number of levels that we need for the work-efficient algorithm
	m_nLevels = 1;
	while (N > 1){
		N /= 2 * LocalWorkSize[0];
		m_nLevels++;
	}
}

CScan::~CScan() 
{
}

bool CScan::InitResources(cl_device_id Device, cl_context Context) {
	//CPU resources
	m_hArray	 = new unsigned int[m_N];
	m_hResultCPU = new unsigned int[m_N];
	m_hResultGPU = new unsigned int[m_N];

	//fill the array with some values
	for(unsigned int i = 0; i < m_N; i++)
		//m_hArray[i] = 1;			// Use this for debugging
		m_hArray[i] = rand() & 15;

	//device resources
	// ping-pong buffers
	cl_int clError, clError2;
	m_dPingArray = clCreateBuffer(Context, CL_MEM_READ_WRITE, sizeof(cl_uint) * m_N, NULL, &clError2);
	clError = clError2;
	m_dPongArray = clCreateBuffer(Context, CL_MEM_READ_WRITE, sizeof(cl_uint) * m_N, NULL, &clError2);
	clError |= clError2;

	// level buffer
	m_dLevelArrays = new cl_mem[m_nLevels];
	unsigned int N = m_N;
	for (unsigned int i = 0; i < m_nLevels; i++) {
		  m_dLevelArrays[i] = clCreateBuffer(Context, CL_MEM_READ_WRITE, sizeof(cl_uint) * N, NULL, &clError2);
		  clError |= clError2;
		  N = max(N / (2 * m_LocalWorkSize[0]), m_LocalWorkSize[0]);
	}
	V_RETURN_FALSE_CL(clError, "Error allocating device arrays");

	  //load and compile kernels
	  char* programCode = NULL;
	  size_t programSize = 0;

	  LoadProgram("Scan.cl", &programCode, &programSize);

	  //create program object
	  m_Program = clCreateProgramWithSource(Context, 1, (const char**) &programCode, &programSize, &clError);
	  V_RETURN_FALSE_CL(clError, "Failed to create program from file.");

	  //build program
	  clError = clBuildProgram(m_Program, 1, &Device, NULL, NULL, NULL);
	  if(clError != CL_SUCCESS) {
		  PrintBuildLog(m_Program, Device);
		  return false;
	  }

	  //create kernels
	  m_ScanNaiveKernel = clCreateKernel(m_Program, "Scan_Naive", &clError);
	  V_RETURN_FALSE_CL(clError, "Failed to create kernel.");

	  m_ScanWorkEfficientKernel = clCreateKernel(m_Program, "Scan_WorkEfficient", &clError);
	  V_RETURN_FALSE_CL(clError, "Failed to create kernel.");

	  m_ScanWorkEfficientAddKernel = clCreateKernel(m_Program, "Scan_WorkEfficientAdd", &clError);
	  V_RETURN_FALSE_CL(clError, "Failed to create kernel.");

	  return true;
  }

  void CScan::ReleaseResources() {
	  // host resources
	  SAFE_DELETE_ARRAY(m_hArray);

	  SAFE_DELETE_ARRAY(m_hResultCPU);
	  SAFE_DELETE_ARRAY(m_hResultGPU);

	  // device resources
	  SAFE_RELEASE_MEMOBJECT(m_dPingArray);
	  SAFE_RELEASE_MEMOBJECT(m_dPongArray);

	  for (unsigned int i = 0; i < m_nLevels; i++) {
		  SAFE_RELEASE_MEMOBJECT(m_dLevelArrays[i]);
	  }
	  SAFE_DELETE_ARRAY(m_dLevelArrays);

	  SAFE_RELEASE_KERNEL(m_ScanNaiveKernel);
	  SAFE_RELEASE_KERNEL(m_ScanWorkEfficientKernel);
	  SAFE_RELEASE_KERNEL(m_ScanWorkEfficientAddKernel);

	  SAFE_RELEASE_PROGRAM(m_Program);
  }

  void CScan::ComputeCPU() {

	  CTimer timer;
	  timer.Start();

	  unsigned int nIterations = 1;
	  for(unsigned int j = 0; j < nIterations; j++) {
		  unsigned int sum = 0;
		  for(unsigned int i = 0; i < m_N; i++) {
			  sum += m_hArray[i];
			  m_hResultCPU[i] = sum; 
		  }
	  }

	  timer.Stop();
	  double ms = 1000 * timer.GetElapsedTime() / double(nIterations);
	  cout << "  average time: " << ms << " ms, throughput: " << 1.0e-6 * (double)m_N / ms << " Gelem/s" <<endl;

  }

  bool CScan::ValidateResults() {
	  if(memcmp(m_hResultCPU, m_hResultGPU, m_N * sizeof(unsigned int)) == 0) {
		  return true;
	  }
	  std::cout << "Gotten Result: ";
	  for (unsigned int i = 0; i < m_N; i++) {
		  //std::cout << m_hResultGPU[i] << ", ";
	  }
	  std::cout << std::endl;
	  std::cout << "Expected Result: ";
	  for (unsigned int i = 0; i < m_N; i++) {
		  //std::cout << m_hResultCPU[i] << ", ";
	  }
	  std::cout << std::endl;
	  return false;
  }



  void CScan::ValidateTask(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3], unsigned int task){

	  cout << "Validating results of task " << task << endl;

	  //run selected task
	  switch (task){
		  case 1:
			  V_RETURN_CL(clEnqueueWriteBuffer(CommandQueue, m_dPingArray, CL_FALSE, 0, m_N * sizeof(cl_uint), m_hArray, 0, NULL, NULL), "Error copying data from host to device!");
			  Scan_Naive(Context, CommandQueue, LocalWorkSize);
			  V_RETURN_CL(clEnqueueReadBuffer(CommandQueue, m_dPingArray, CL_TRUE, 0, m_N * sizeof(cl_uint), m_hResultGPU, 0, NULL, NULL), "Error reading data from device!");
			  break;
		  case 2:
			  V_RETURN_CL(clEnqueueWriteBuffer(CommandQueue, m_dLevelArrays[0], CL_FALSE, 0, m_N * sizeof(cl_uint), m_hArray, 0, NULL, NULL), "Error copying data from host to device!");
			  Scan_WorkEfficient(Context, CommandQueue, LocalWorkSize);
			  V_RETURN_CL(clEnqueueReadBuffer(CommandQueue, m_dLevelArrays[0], CL_TRUE, 0, m_N * sizeof(cl_uint), m_hResultGPU, 0, NULL, NULL), "Error reading data from device!");
			  break;
	  }

	  if(ValidateResults())
		  cout<<"  TEST PASSED!"<<endl;
	  else
		  cout<<"  INVALID RESULTS!"<<endl;
  }


  void CScan::TestPerformance(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3], unsigned int task){

	  cout << "Testing performance of task " << task << endl;

	  //write input data to the GPU
	  V_RETURN_CL(clEnqueueWriteBuffer(CommandQueue, m_dPingArray, CL_FALSE, 0, m_N * sizeof(cl_uint), m_hArray, 0, NULL, NULL), "Error copying data from host to device!");
	  //finish all before we start meassuring the time
	  V_RETURN_CL(clFinish(CommandQueue), "Error finishing the queue!");

	  CTimer timer;
	  timer.Start();

	  //run the kernel N times
	  unsigned int nIterations = 100;
	  for(unsigned int i = 0; i < nIterations; i++) {
		  //run selected task
		  switch (task){
			  case 1:
				  Scan_Naive(Context, CommandQueue, LocalWorkSize);
				  break;
			  case 2:
				  Scan_WorkEfficient(Context, CommandQueue, LocalWorkSize);
				  break;
		  }
	  }

	  //wait until the command queue is empty again
	  V_RETURN_CL(clFinish(CommandQueue), "Error finishing the queue!");

	  timer.Stop();

	  double ms = 1000 * timer.GetElapsedTime() / double(nIterations);
	  cout << "  average time: " << ms << " ms, throughput: " << 1.0e-6 * (double)m_N / ms << " Gelem/s" <<endl;
  }



  void CScan::ComputeGPU(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3]){

	  cout << endl;

	  ValidateTask(Context, CommandQueue, LocalWorkSize, 1);
	  ValidateTask(Context, CommandQueue, LocalWorkSize, 2);

	  cout << endl;

	  TestPerformance(Context, CommandQueue, LocalWorkSize, 1);
	  TestPerformance(Context, CommandQueue, LocalWorkSize, 2);

	  cout << endl;
  }


  void CScan::Scan_Naive(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3]) {
	  // define local variables
	  cl_int clErr;
	  size_t globalWorkSize[1];
	  size_t localWorkSize[1];
	  cl_mem swap_tmp;
	  
	  // set kernel arguments that dont change
	  clErr = clSetKernelArg(m_ScanNaiveKernel, 2, sizeof(cl_uint), (void*) &m_N);
	  V_RETURN_CL(clErr, "Error setting Kernel Argument: N");
	  
	  // get local and global work size (they dont change)
	  localWorkSize[0] = LocalWorkSize[0];
	  globalWorkSize[0] = GetGlobalWorkSize(m_N, localWorkSize[0]);

	  for (unsigned int offset = 1; offset < m_N; offset <<= 1) {
		  // set kernel arguments		
		  clErr = clSetKernelArg(m_ScanNaiveKernel, 0, sizeof(cl_mem), (void*) &m_dPingArray);
		  V_RETURN_CL(clErr, "Error setting Kernel Argument: PingArray");
		  clErr = clSetKernelArg(m_ScanNaiveKernel, 1, sizeof(cl_mem), (void*) &m_dPongArray);
		  V_RETURN_CL(clErr, "Error setting Kernel Argument: PongArray");
		  clErr = clSetKernelArg(m_ScanNaiveKernel, 3, sizeof(cl_uint), (void*) &offset);
		  V_RETURN_CL(clErr, "Error setting Kernel Argument: offset");
	  
		  // enqueue kernel
		  clErr = clEnqueueNDRangeKernel(CommandQueue, m_ScanNaiveKernel, 1, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
		  V_RETURN_CL(clErr, "Error enqueing kernel");

		  // switch ping pong arrays
		  swap_tmp = m_dPingArray;
		  m_dPingArray = m_dPongArray;
		  m_dPongArray = swap_tmp;
	  }
  }

#define NUM_BANKS	32

void CScan::Scan_WorkEfficient(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3]) {


	  // Make sure that the local prefix sum works before you start experimenting with large arrays
	  cl_int clErr;
	  size_t globalWorkSize[1];
	  size_t localWorkSize[1];

	  unsigned int numThreads = m_N;
	  for(unsigned int level = 0; level < m_nLevels-1; level++) {
		  numThreads >>= 1;
		  if (numThreads == 0) {
			  std::cout << "!!!!!!!!!!! MEGA FAIL !!!!!!!!!!!!!" << std::endl;
			  break;
		  }
		  // get local and global work size (they dont change)
		  localWorkSize[0] = LocalWorkSize[0];
		  globalWorkSize[0] = GetGlobalWorkSize(numThreads, localWorkSize[0]);
		  if (globalWorkSize[0] > numThreads) {
			  globalWorkSize[0] = numThreads;
		  }
		  if (localWorkSize[0] > numThreads) {
			  localWorkSize[0] = numThreads;
		  }

		  // set kernel arguments
		  clErr = clSetKernelArg(m_ScanWorkEfficientKernel, 0, sizeof(cl_mem), (void*) &m_dLevelArrays[level]);
		  V_RETURN_CL(clErr, "Error setting Kernel Argument: array");
		  clErr = clSetKernelArg(m_ScanWorkEfficientKernel, 1, sizeof(cl_mem), (void*) &m_dLevelArrays[level+1]);
		  V_RETURN_CL(clErr, "Error setting Kernel Argument: higherLevelArray");
		  size_t localBlockSize = sizeof(cl_uint)*localWorkSize[0]*2;
		  localBlockSize += (localBlockSize/NUM_BANKS);
		  clErr = clSetKernelArg(m_ScanWorkEfficientKernel, 2, localBlockSize, NULL);
		  V_RETURN_CL(clErr, "Error setting Kernel Argument: local memory block");
	  
		  // enqueue kernel
		  clErr = clEnqueueNDRangeKernel(CommandQueue, m_ScanWorkEfficientKernel, 1, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
		  V_RETURN_CL(clErr, "Error enqueing kernel");

		  if (level < m_nLevels-2)
			  numThreads /= localWorkSize[0];
	  }
	  // backwards adding groups up
	  for(int level = m_nLevels-3; level >= 0; level--) {
		  // get local and global work size (they dont change)
		  localWorkSize[0] = LocalWorkSize[0];
		  numThreads *= (2*localWorkSize[0]);
		  globalWorkSize[0] = GetGlobalWorkSize(numThreads, localWorkSize[0]);
		  if (globalWorkSize[0] > numThreads) {
			  globalWorkSize[0] = numThreads;
		  }
		  if (localWorkSize[0] > numThreads) {
			  localWorkSize[0] = numThreads;
		  }


		// set kernel arguments
		clErr = clSetKernelArg(m_ScanWorkEfficientAddKernel, 0, sizeof(cl_mem), (void*) &m_dLevelArrays[level+1]);
		V_RETURN_CL(clErr, "Error setting Kernel Argument: array");
		clErr = clSetKernelArg(m_ScanWorkEfficientAddKernel, 1, sizeof(cl_mem), (void*) &m_dLevelArrays[level]);
		V_RETURN_CL(clErr, "Error setting Kernel Argument: higherLevelArray");
		size_t localBlockSize = sizeof(cl_uint)*localWorkSize[0]*2;
		localBlockSize += (localBlockSize/NUM_BANKS);
		clErr = clSetKernelArg(m_ScanWorkEfficientAddKernel, 2, localBlockSize, NULL);
		V_RETURN_CL(clErr, "Error setting Kernel Argument: local memory block");
	
		// enqueue kernel
		clErr = clEnqueueNDRangeKernel(CommandQueue, m_ScanWorkEfficientAddKernel, 1, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
		V_RETURN_CL(clErr, "Error enqueing kernel");

	}
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
