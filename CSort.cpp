#include "CSort.h"
#include "CTimer.h"
#include "Common.h"
#include <algorithm>
#include <time.h>

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CSort

CSort::CSort(unsigned int N) 
	: m_N(N), m_hInput(NULL), 
	m_dArray(NULL),
	m_Program(NULL), 
	m_OddEvenMergesortKernel(NULL)
{
}

CSort::~CSort() 
{
}

bool CSort::InitResources(cl_device_id Device, cl_context Context) {
	//CPU resources
	m_hInput = new unsigned int[m_N];
	m_resultCPU = new unsigned int[m_N];
	m_resultGPU = new unsigned int[m_N];

	srand ( time(NULL) );
	//srand(1);
	//fill the array with some values
	for(unsigned int i = 0; i < m_N; i++) 
		//m_hInput[i] = 1;			// Use this for debugging
		m_hInput[i] = rand() & 1023;
		//m_hInput[i] = rand();
		


	
	cl_int clError;
	
	
	//device resources
	m_dArray = clCreateBuffer(Context, CL_MEM_READ_WRITE, sizeof(cl_uint) * m_N, NULL, &clError);
	V_RETURN_FALSE_CL(clError, "Error allocating device arrays");
	

	

	//load and compile kernels
	char* programCode = NULL;
	size_t programSize = 0;

	LoadProgram("Sort.cl", &programCode, &programSize);

	//create program object
	m_Program = clCreateProgramWithSource(Context, 1, (const char**) &programCode, &programSize, &clError);
	V_RETURN_FALSE_CL(clError, "Failed to create program from file.");

	//build program
	clError = clBuildProgram(m_Program, 1, &Device, NULL, NULL, NULL);
	if(clError != CL_SUCCESS) {
		PrintBuildLog(m_Program, Device);
		return false;
	}

	//create kernels
	m_OddEvenMergesortKernel = clCreateKernel(m_Program, "OddEvenMergesort", &clError);
	V_RETURN_FALSE_CL(clError, "Failed to create kernel.");
	
	m_LocalMergeKernel = clCreateKernel(m_Program, "LocalMerge", &clError);
	V_RETURN_FALSE_CL(clError, "Failed to create kernel.");
	
	m_SearchSplittersKernel = clCreateKernel(m_Program, "SearchSplitters", &clError);
	V_RETURN_FALSE_CL(clError, "Failed to create kernel.");
	
	m_LocalSplitterMergeKernel = clCreateKernel(m_Program, "LocalSplitterMerge", &clError);
	V_RETURN_FALSE_CL(clError, "Failed to create kernel.");
	
	m_SampleSplitterAB = clCreateKernel(m_Program, "SampleSplitterAB", &clError);
	V_RETURN_FALSE_CL(clError, "Failed to create kernel.");
	
	m_SplitterMergeKernel = clCreateKernel(m_Program, "SplitterMerge", &clError);
	V_RETURN_FALSE_CL(clError, "Failed to create kernel.");
	
	m_SlowBinaryMergeKernel = clCreateKernel(m_Program, "SlowBinaryMerge", &clError);
	V_RETURN_FALSE_CL(clError, "Failed to create kernel.");
	
	
	
	return true;
}

void CSort::ReleaseResources() {
	// host resources
	SAFE_DELETE_ARRAY(m_hInput);
	SAFE_DELETE_ARRAY(m_resultCPU);
	SAFE_DELETE_ARRAY(m_resultGPU);

	// device resources
	SAFE_RELEASE_MEMOBJECT(m_dArray);

	SAFE_RELEASE_KERNEL(m_OddEvenMergesortKernel);
	SAFE_RELEASE_KERNEL(m_LocalMergeKernel);
	SAFE_RELEASE_KERNEL(m_LocalSplitterMergeKernel);
	SAFE_RELEASE_KERNEL(m_SearchSplittersKernel);
	SAFE_RELEASE_KERNEL(m_SampleSplitterAB);
	SAFE_RELEASE_KERNEL(m_SplitterMergeKernel);
	SAFE_RELEASE_KERNEL(m_SlowBinaryMergeKernel);
	
	SAFE_RELEASE_PROGRAM(m_Program);
}

void CSort::ComputeCPU() {

	CTimer timer;
	timer.Start();

	unsigned int nIterations = 10;
	for(unsigned int j = 0; j < nIterations; j++) {
		// copy data
		for(unsigned int i = 0; i < m_N; i++) 
			m_resultCPU[i] = m_hInput[i];
		std::sort(m_resultCPU, m_resultCPU+m_N);
	}

	timer.Stop();
	double ms = 1000 * timer.GetElapsedTime() / double(nIterations);
	cout << "  average time: " << ms << " ms, throughput: " << 1.0e-6 * (double)m_N / ms << " Gelem/s" <<endl;
	
	// print results
	std::cout << "CPU RESULTS:" << std::endl;
	for(unsigned int i = 0; i < m_N; i++) {
		//std::cout << m_resultCPU[i] << ",";
	}
	std::cout << std::endl;
	std::cout << std::endl;
}

bool CSort::ValidateResults() {
	  if(memcmp(m_resultCPU, m_resultGPU, m_N * sizeof(unsigned int)) == 0) {
		  return true;
	  }
	  std::cout << "Gotten Result: ";
	  for (unsigned int i = 0; i < m_N; i++) {
		std::cout << m_resultGPU[i] << ", ";
	  }
	  std::cout << std::endl;
	  std::cout << "Expected Result: ";
	  for (unsigned int i = 0; i < m_N; i++) {
		std::cout << m_resultCPU[i] << ", ";
	  }
	  std::cout << std::endl;
	  return false;
}



void CSort::ValidateTask(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3], unsigned int task){

	cout << "Validating results of task " << task << endl;

	//write input data to the GPU
	V_RETURN_CL(clEnqueueWriteBuffer(CommandQueue, m_dArray, CL_FALSE, 0, m_N * sizeof(cl_uint), m_hInput, 0, NULL, NULL), "Error copying data from host to device!");
	
	// TODO remove this:
	std::cout << "localworksize: " << LocalWorkSize[0] << std::endl;
	
	//run selected task
	switch (task){
		case 1:
			Sort_MergeSort(Context, CommandQueue, LocalWorkSize);
			break;
		case 2:
			Sort_SlowMergeSort(Context, CommandQueue, LocalWorkSize);
			break;
		case 4:
			Sort_OddEvenMergesort(Context, CommandQueue, LocalWorkSize);
			Sort_LocalMerge(Context, CommandQueue, LocalWorkSize);
			break;
		case 3:
			Sort_OddEvenMergesort(Context, CommandQueue, LocalWorkSize);
			Sort_LocalMerge(Context, CommandQueue, LocalWorkSize);
			Sort_SampleSplitterAB(Context, CommandQueue, LocalWorkSize, LocalWorkSize[0]);
			break;
	}

	//read back the results synchronously.
	V_RETURN_CL(clEnqueueReadBuffer(CommandQueue, m_dArray, CL_TRUE, 0, m_N * sizeof(cl_uint), m_resultGPU, 0, NULL, NULL), "Error reading data from device!");

	if(ValidateResults())
		cout<<"  TEST PASSED!"<<endl;
	else
		cout<<"  INVALID RESULTS!"<<endl;
}


void CSort::TestPerformance(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3], unsigned int task){

	//cout << "Testing performance of task " << task << endl;

	//write input data to the GPU
	V_RETURN_CL(clEnqueueWriteBuffer(CommandQueue, m_dArray, CL_FALSE, 0, m_N * sizeof(cl_uint), m_hInput, 0, NULL, NULL), "Error copying data from host to device!");
	//finish all before we start meassuring the time
	V_RETURN_CL(clFinish(CommandQueue), "Error finishing the queue!");

	CTimer timer;
	timer.Start();

	//run the kernel N times
	unsigned int nIterations = 10;
	for(unsigned int i = 0; i < nIterations; i++) {
		//run selected task
		switch (task){
			case 1:
				Sort_MergeSort(Context, CommandQueue, LocalWorkSize);
				break;
			case 2:
				Sort_SlowMergeSort(Context, CommandQueue, LocalWorkSize);
				break;
		}
	}

	//wait until the command queue is empty again
	V_RETURN_CL(clFinish(CommandQueue), "Error finishing the queue!");

	timer.Stop();

	double ms = 1000 * timer.GetElapsedTime() / double(nIterations);
	//cout << "  average time: " << ms << " ms, throughput: " << 1.0e-6 * (double)m_N / ms << " Gelem/s" <<endl;
	cout << ms;
}



void CSort::ComputeGPU(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3]){

	cout << endl;

	ValidateTask(Context, CommandQueue, LocalWorkSize, 1);
	ValidateTask(Context, CommandQueue, LocalWorkSize, 2);

	cout << endl;

	TestPerformance(Context, CommandQueue, LocalWorkSize, 1);
	TestPerformance(Context, CommandQueue, LocalWorkSize, 2);

	cout << endl;
}


void CSort::Sort_OddEvenMergesort(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3]) {

	cl_int clErr;
	size_t globalWorkSize[1];
	size_t localWorkSize[1];
	localWorkSize[0] = LocalWorkSize[0];
	

	clErr = clSetKernelArg(m_OddEvenMergesortKernel, 0, sizeof(cl_mem), (void*) &m_dArray);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: array");
	
	// set kernel arguments
	clErr = clSetKernelArg(m_OddEvenMergesortKernel, 1, sizeof(cl_uint), (void*) &m_N);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: size");
	clErr = clSetKernelArg(m_OddEvenMergesortKernel, 2, sizeof(cl_uint)*localWorkSize[0], NULL);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: localBlock");

	// get global and local work size in dependence of localThreads
	globalWorkSize[0] = GetGlobalWorkSize(m_N, localWorkSize[0]);

	// execute the kernel
	clErr = clEnqueueNDRangeKernel(CommandQueue, m_OddEvenMergesortKernel, 1, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
}

void CSort::Sort_LocalMerge(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3]) {

	cl_int clErr;
	size_t globalWorkSize[1];
	size_t localWorkSize[1];
	localWorkSize[0] = LocalWorkSize[0];
	

	clErr = clSetKernelArg(m_LocalMergeKernel, 0, sizeof(cl_mem), (void*) &m_dArray);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: array");
	
	// set kernel arguments
	clErr = clSetKernelArg(m_LocalMergeKernel, 1, sizeof(cl_uint), (void*) &m_N);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: size");
	clErr = clSetKernelArg(m_LocalMergeKernel, 2, sizeof(cl_uint)*localWorkSize[0], NULL);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: localBlockA");
	clErr = clSetKernelArg(m_LocalMergeKernel, 3, sizeof(cl_uint)*localWorkSize[0], NULL);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: localBlockB");
	clErr = clSetKernelArg(m_LocalMergeKernel, 4, sizeof(cl_uint)*2*localWorkSize[0], NULL);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: localBlockResult");

	// get global and local work size in dependence of localThreads
	globalWorkSize[0] = GetGlobalWorkSize(m_N/2, localWorkSize[0]);

	// execute the kernel
	clErr = clEnqueueNDRangeKernel(CommandQueue, m_LocalMergeKernel, 1, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
}

void CSort::Sort_LocalSplitterMerge(cl_context Context, cl_command_queue CommandQueue, cl_mem& splitterAB, cl_mem& mergedSplitter, size_t numSplitter, size_t splitterSeqSize) {

	cl_int clErr;
	size_t globalWorkSize[1];
	size_t localWorkSize[1];
	localWorkSize[0] = splitterSeqSize;
	globalWorkSize[0] = numSplitter/2;

	clErr = clSetKernelArg(m_LocalSplitterMergeKernel, 0, sizeof(cl_mem), (void*) &splitterAB);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: splitterAB");
	clErr = clSetKernelArg(m_LocalSplitterMergeKernel, 1, sizeof(cl_mem), (void*) &mergedSplitter);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: mergedSplitter");
	
	// set kernel arguments
	clErr = clSetKernelArg(m_LocalSplitterMergeKernel, 2, sizeof(cl_uint), (void*) &numSplitter);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: size");
	clErr = clSetKernelArg(m_LocalSplitterMergeKernel, 3, sizeof(cl_uint)*localWorkSize[0], NULL);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: localBlockA");
	clErr = clSetKernelArg(m_LocalSplitterMergeKernel, 4, sizeof(cl_uint)*localWorkSize[0], NULL);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: localBlockB");
	clErr = clSetKernelArg(m_LocalSplitterMergeKernel, 5, sizeof(cl_uint)*2*localWorkSize[0], NULL);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: localBlockResult");


	// get global and local work size in dependence of localThreads
	//globalWorkSize[0] = GetGlobalWorkSize(m_N/2, localWorkSize[0]);

	// execute the kernel
	clErr = clEnqueueNDRangeKernel(CommandQueue, m_LocalSplitterMergeKernel, 1, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
}


void CSort::Sort_MergeSort(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3])
{
	size_t localWorkSize[1];
	localWorkSize[0] = LocalWorkSize[0];
	size_t t = localWorkSize[0];
	

	
	// 1.) odd even mergesort to sort sequences of size t
	Sort_OddEvenMergesort(Context, CommandQueue, LocalWorkSize);
	
	// define number of sequences and seqlength
	unsigned int numSequences = m_N / t;
	unsigned int seqLength = t;
	
	
	// 2.) local merge, creating sequences of size 2*t
	if (numSequences > 1) {
		Sort_LocalMerge(Context, CommandQueue, LocalWorkSize);
		numSequences /= 2;
		seqLength *= 2;
	}
	
	// init some buffers
	unsigned int numSplitter = m_N / t;
	cl_int clErr;
	cl_mem splitters = clCreateBuffer(Context, CL_MEM_READ_WRITE, 2 * sizeof(cl_uint) * numSplitter, NULL, &clErr);
	V_RETURN_CL(clErr, "Error allocating device array splitters");
	
	// init ping and pong arrays
	cl_mem pingArray = m_dArray;
	cl_mem pongArray = clCreateBuffer(Context, CL_MEM_READ_WRITE, sizeof(cl_uint) * m_N, NULL, &clErr);
	V_RETURN_CL(clErr, "Error allocating device array pong-array");
	
	// now we have 'numSequences' sequences of length t
	for (; numSequences > 1; numSequences /= 2) {
		
		//std::cout << "NumSequences=" << numSequences << "  seqLength=" << seqLength << std::endl;
		
		// 1. get splitters
		getSplitters(Context, CommandQueue, pingArray, splitters, numSequences, seqLength, t, localWorkSize[0]);
		
		// 2. merge
		splitterMerge(Context, CommandQueue, localWorkSize[0], pingArray, pongArray, m_N, splitters, seqLength, false);
		
		// 3. switch ping pong arrays
		cl_mem tmp = pingArray;
		pingArray = pongArray;
		pongArray = tmp;
		
		seqLength *= 2;
	}
	
	m_dArray = pingArray;
	SAFE_RELEASE_MEMOBJECT(pongArray);
	
}


void CSort::Sort_SlowMergeSort(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3])
{
	size_t localWorkSize[1];
	localWorkSize[0] = LocalWorkSize[0];
	size_t t = localWorkSize[0];
	

	
	// 1.) odd even mergesort to sort sequences of size t
	Sort_OddEvenMergesort(Context, CommandQueue, LocalWorkSize);
	
	// define number of sequences and seqlength
	unsigned int numSequences = m_N / t;
	unsigned int seqLength = t;
	
	// init ping and pong arrays
	cl_int clErr;
	cl_mem pingArray = m_dArray;
	cl_mem pongArray = clCreateBuffer(Context, CL_MEM_READ_WRITE, sizeof(cl_uint) * m_N, NULL, &clErr);
	V_RETURN_CL(clErr, "Error allocating device array pong-array");
	
	// now we have 'numSequences' sequences of length t
	for (; numSequences > 1; numSequences /= 2) {
		
		//std::cout << "NumSequences=" << numSequences << "  seqLength=" << seqLength << std::endl;
		
		slowBinaryMerge(Context, CommandQueue, localWorkSize[0], pingArray, pongArray, m_N, seqLength);
		
		// 3. switch ping pong arrays
		cl_mem tmp = pingArray;
		pingArray = pongArray;
		pongArray = tmp;
		
		seqLength *= 2;
	}
	
	m_dArray = pingArray;
	SAFE_RELEASE_MEMOBJECT(pongArray);
	
}


void CSort::slowBinaryMerge(cl_context Context, cl_command_queue CommandQueue, size_t localWorkSize, cl_mem& inArray, cl_mem& outArray, unsigned int size, unsigned int seqSize) {
	cl_int clErr;
	clErr = clSetKernelArg(m_SlowBinaryMergeKernel, 0, sizeof(cl_mem), (void*) &inArray);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: array");
	
	clErr = clSetKernelArg(m_SlowBinaryMergeKernel, 1, sizeof(cl_mem), (void*) &outArray);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: array");
	
	clErr = clSetKernelArg(m_SlowBinaryMergeKernel, 2, sizeof(cl_uint), (void*) &size);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: size");
	
	clErr = clSetKernelArg(m_SlowBinaryMergeKernel, 3, sizeof(cl_uint), (void*)&seqSize);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: seqSize");
	
	// get global and local work size in dependence of localThreads
	size_t globalWorkSize = GetGlobalWorkSize(size/seqSize, localWorkSize);

	// execute the kernel
	clErr = clEnqueueNDRangeKernel(CommandQueue, m_SlowBinaryMergeKernel, 1, NULL, &globalWorkSize, &localWorkSize, 0, NULL, NULL);
}



void CSort::splitterMerge(cl_context Context, cl_command_queue CommandQueue, size_t localWorkSize, cl_mem& inArray, cl_mem& outArray, unsigned int size, cl_mem& splitters, unsigned int seqSize, bool indexOutput) {
	cl_int clErr;
	clErr = clSetKernelArg(m_SplitterMergeKernel, 0, sizeof(cl_mem), (void*) &inArray);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: array");
	
	clErr = clSetKernelArg(m_SplitterMergeKernel, 1, sizeof(cl_mem), (void*) &outArray);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: array");
	
	clErr = clSetKernelArg(m_SplitterMergeKernel, 2, sizeof(cl_uint), (void*) &size);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: size");
	
	clErr = clSetKernelArg(m_SplitterMergeKernel, 3, sizeof(cl_mem), (void*) &splitters);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: splitters");
	
	clErr = clSetKernelArg(m_SplitterMergeKernel, 4, sizeof(cl_uint), (void*)&seqSize);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: seqSize");
	
	clErr = clSetKernelArg(m_SplitterMergeKernel, 5, sizeof(cl_uint)*localWorkSize, NULL);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: localBlockA");
	clErr = clSetKernelArg(m_SplitterMergeKernel, 6, sizeof(cl_uint)*localWorkSize, NULL);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: localBlockB");
	clErr = clSetKernelArg(m_SplitterMergeKernel, 7, sizeof(cl_uint)*2*localWorkSize, NULL);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: localBlockResult");
	
	cl_uint indexOut = 0;
	if (indexOutput) indexOut = 1;
	clErr = clSetKernelArg(m_SplitterMergeKernel, 8, sizeof(cl_uint), (void*)&indexOut);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: indexOutput");
	
	// get global and local work size in dependence of localThreads
	size_t globalWorkSize = GetGlobalWorkSize(size, localWorkSize);

	// execute the kernel
	clErr = clEnqueueNDRangeKernel(CommandQueue, m_SplitterMergeKernel, 1, NULL, &globalWorkSize, &localWorkSize, 0, NULL, NULL);
}

void CSort::getSplitters(cl_context Context, cl_command_queue CommandQueue, cl_mem& dArray, cl_mem& splitters, unsigned int numSequences, unsigned int seqLength, size_t t, size_t localWorkSize) {
	cl_int clErr;
	
	/* get AB splitters */
	
	unsigned int numSplitter = seqLength*numSequences / t;
	
	cl_mem dSplitterAB = clCreateBuffer(Context, CL_MEM_READ_WRITE, sizeof(cl_uint) * numSplitter, NULL, &clErr);
	V_RETURN_CL(clErr, "Error allocating device array dSplitterAB");
	
	calcSplittersAB(Context, CommandQueue, localWorkSize, numSplitter, t, dArray, dSplitterAB);
	
	
	
	/* merge AB splitters */
	
	unsigned int splitterSeqSize = seqLength / t;
	
	cl_mem mergedSplitter = clCreateBuffer(Context, CL_MEM_READ_WRITE, sizeof(cl_uint) * numSplitter, NULL, &clErr);
	V_RETURN_CL(clErr, "Error allocating device array mergedSplitter");
	
	mergeSplittersAB(Context, CommandQueue, localWorkSize, numSplitter, splitterSeqSize, dSplitterAB, mergedSplitter);
	
	
	
	/* search for real splitting indeces */
	
	searchSplitters(Context, CommandQueue, localWorkSize, dArray, dSplitterAB, mergedSplitter, splitters, numSplitter, splitterSeqSize, t);
}

/*
 * Samples every t-th element from the array 'dArray' and saves
 * the samples into the array 'dSplitterAB'.
 */
void CSort::calcSplittersAB(cl_context Context, cl_command_queue CommandQueue, size_t localWorkSize, unsigned int numSplitter, size_t t, cl_mem& dArray, cl_mem& dSplitterAB) {
	cl_int clErr;
	
	// set kernel arguments
	clErr = clSetKernelArg(m_SampleSplitterAB, 0, sizeof(cl_mem), (void*) &dArray);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: array");
	clErr = clSetKernelArg(m_SampleSplitterAB, 1, sizeof(cl_mem), (void*) &dSplitterAB);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: splitterAB");
	clErr = clSetKernelArg(m_SampleSplitterAB, 2, sizeof(cl_uint), (void*)&numSplitter);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: numSplitter");
	clErr = clSetKernelArg(m_SampleSplitterAB, 3, sizeof(cl_uint), (void*)&t);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: t");

	// get global and local work size in dependence of localThreads
	size_t globalWorkSize = GetGlobalWorkSize(numSplitter, localWorkSize);

	// execute the kernel
	clErr = clEnqueueNDRangeKernel(CommandQueue, m_SampleSplitterAB, 1, NULL, &globalWorkSize, &localWorkSize, 0, NULL, NULL);
}

void CSort::mergeSplittersAB(cl_context Context, cl_command_queue CommandQueue, size_t localWorkSize, unsigned int numSplitter, unsigned int splitterSeqSize, cl_mem& dSplitterAB, cl_mem& dMergedSplitters) {
	cl_int clErr;
	
	
	if (splitterSeqSize <= localWorkSize) {
		Sort_LocalSplitterMerge(Context, CommandQueue, dSplitterAB, dMergedSplitters, numSplitter, splitterSeqSize); // die 2 ist hier die splitterSequenzGroesse
	} else {
		size_t t = localWorkSize;
		
		unsigned int numSplitterSplitter = numSplitter / t;
		
		cl_mem splitterSplitters = clCreateBuffer(Context, CL_MEM_READ_WRITE, 2 * sizeof(cl_uint) * numSplitterSplitter, NULL, &clErr);
		V_RETURN_CL(clErr, "Error allocating device array splitters");
		
		unsigned int numSequences = numSplitter / splitterSeqSize;
		unsigned int seqLength = splitterSeqSize;
		
		// 1. get splitters
		getSplitters(Context, CommandQueue, dSplitterAB, splitterSplitters, numSequences, seqLength, t, localWorkSize);
		
		// 2. merge
		splitterMerge(Context, CommandQueue, localWorkSize, dSplitterAB, dMergedSplitters, numSplitter, splitterSplitters, seqLength, true);
	}
}


void CSort::searchSplitters(cl_context Context, cl_command_queue CommandQueue, size_t localWorkSize, cl_mem& dArray, cl_mem& dSplitterAB, cl_mem& dMergedSplitters, cl_mem& splitters, unsigned int numSplitters, unsigned int splitterSeqSize, size_t t) {
	cl_int clErr;
	
	clErr = clSetKernelArg(m_SearchSplittersKernel, 0, sizeof(cl_mem), (void*) &dArray);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: array");
	
	clErr = clSetKernelArg(m_SearchSplittersKernel, 1, sizeof(cl_mem), (void*) &dSplitterAB);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: splitterAB");
	
	clErr = clSetKernelArg(m_SearchSplittersKernel, 2, sizeof(cl_mem), (void*) &dMergedSplitters);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: mergedSplitter");
	
	clErr = clSetKernelArg(m_SearchSplittersKernel, 3, sizeof(cl_mem), (void*) &splitters);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: splitters");
	
	clErr = clSetKernelArg(m_SearchSplittersKernel, 4, sizeof(cl_uint), (void*)&numSplitters);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: numSplitter");

	clErr = clSetKernelArg(m_SearchSplittersKernel, 5, sizeof(cl_uint), (void*)&splitterSeqSize);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: splitterSeqSize");

	clErr = clSetKernelArg(m_SearchSplittersKernel, 6, sizeof(cl_uint), (void*)&t);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: t");
	
	// get global and local work size in dependence of localThreads
	size_t globalWorkSize = GetGlobalWorkSize(numSplitters, localWorkSize);

	// execute the kernel
	clErr = clEnqueueNDRangeKernel(CommandQueue, m_SearchSplittersKernel, 1, NULL, &globalWorkSize, &localWorkSize, 0, NULL, NULL);
}


void CSort::Sort_SampleSplitterAB(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3], size_t t) {

	cl_int clErr;
	size_t globalWorkSize[1];
	size_t localWorkSize[1];
	localWorkSize[0] = LocalWorkSize[0];
	
	// kernel SampleSplitterAB(__global uint* array, uint size, __global uint* splitterAB, uint numSplitter, uint t)
	cl_uint numSplitter = m_N / t;
	
	std::cout << "numSplitter: " << numSplitter << std::endl;
	
	m_dSplitterAB = clCreateBuffer(Context, CL_MEM_READ_WRITE, sizeof(cl_uint) * numSplitter, NULL, &clErr);
	V_RETURN_CL(clErr, "Error allocating device array m_dSplitterAB");

	
	
	clErr = clSetKernelArg(m_SampleSplitterAB, 0, sizeof(cl_mem), (void*) &m_dArray);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: array");
	
	// set kernel arguments
	clErr = clSetKernelArg(m_SampleSplitterAB, 1, sizeof(cl_mem), (void*) &m_dSplitterAB);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: splitterAB");
	
	
	clErr = clSetKernelArg(m_SampleSplitterAB, 2, sizeof(cl_uint), (void*)&numSplitter);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: numSplitter");
	clErr = clSetKernelArg(m_SampleSplitterAB, 3, sizeof(cl_uint), (void*)&t);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: t");


	// get global and local work size in dependence of localThreads
	globalWorkSize[0] = GetGlobalWorkSize(numSplitter, localWorkSize[0]);

	// execute the kernel
	clErr = clEnqueueNDRangeKernel(CommandQueue, m_SampleSplitterAB, 1, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
	
	//read back the results synchronously.
	unsigned int * debugSplitterAB = new unsigned int[numSplitter];
	V_RETURN_CL(clEnqueueReadBuffer(CommandQueue, m_dSplitterAB, CL_TRUE, 0,  sizeof(cl_uint) * numSplitter, debugSplitterAB, 0, NULL, NULL), "Error reading data from device!");
	
	std::cout << "SplitterAB:";
	for (unsigned int i = 0; i < numSplitter; ++i) {
		std::cout << debugSplitterAB[i] << ", ";
	}
	std::cout << std::endl;
	std::cout << std::endl;
	
	
	/* ******************
	 * Merge Splitters
	 ********************/
	
	cl_mem mergedSplitter = clCreateBuffer(Context, CL_MEM_READ_WRITE, sizeof(cl_uint) * numSplitter, NULL, &clErr);
	V_RETURN_CL(clErr, "Error allocating device array mergedSplitter");
	
	Sort_LocalSplitterMerge(Context, CommandQueue, m_dSplitterAB, mergedSplitter, numSplitter, 2); // die 2 ist hier die splitterSequenzGroesse
	
	unsigned int * debugMergedSplitter = new unsigned int[numSplitter];
	V_RETURN_CL(clEnqueueReadBuffer(CommandQueue, mergedSplitter, CL_TRUE, 0,  sizeof(cl_uint) * numSplitter, debugMergedSplitter, 0, NULL, NULL), "Error reading data from device!");
	
	std::cout << "MergedSplitter:";
	for (unsigned int i = 0; i < numSplitter; ++i) {
		std::cout << debugMergedSplitter[i] << ", ";
	}
	std::cout << std::endl;
	std::cout << std::endl;
	
	
	/* **********************
	 *  Find real splitters
	 ************************/
	
	
	// __kernel void SearchSplitters(__global uint* array, uint size, __global uint* splittersAB, __global uint* mergedSplitters, __global uint* splitters, uint numSplitters, uint splittersSeqSize, uint t, __local uint* localBlock)
	
	cl_mem splitters = clCreateBuffer(Context, CL_MEM_READ_WRITE, 2 * sizeof(cl_uint) * numSplitter, NULL, &clErr);
	V_RETURN_CL(clErr, "Error allocating device array splitters");
	
	clErr = clSetKernelArg(m_SearchSplittersKernel, 0, sizeof(cl_mem), (void*) &m_dArray);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: array");
	
	clErr = clSetKernelArg(m_SearchSplittersKernel, 1, sizeof(cl_mem), (void*) &m_dSplitterAB);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: splitterAB");
	
	clErr = clSetKernelArg(m_SearchSplittersKernel, 2, sizeof(cl_mem), (void*) &mergedSplitter);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: mergedSplitter");
	
	clErr = clSetKernelArg(m_SearchSplittersKernel, 3, sizeof(cl_mem), (void*) &splitters);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: splitters");
	
	clErr = clSetKernelArg(m_SearchSplittersKernel, 4, sizeof(cl_uint), (void*)&numSplitter);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: numSplitter");
	cl_uint splitterSeqSize = 2; // TODO
	clErr = clSetKernelArg(m_SearchSplittersKernel, 5, sizeof(cl_uint), (void*)&splitterSeqSize);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: splitterSeqSize");
	// TODO
	clErr = clSetKernelArg(m_SearchSplittersKernel, 6, sizeof(cl_uint), (void*)&t);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: t");
	
	clErr = clSetKernelArg(m_SearchSplittersKernel, 7, sizeof(cl_uint)*LocalWorkSize[0], NULL);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: localBlock");
	
	// get global and local work size in dependence of localThreads
	globalWorkSize[0] = GetGlobalWorkSize(numSplitter, LocalWorkSize[0]);

	// execute the kernel
	clErr = clEnqueueNDRangeKernel(CommandQueue, m_SearchSplittersKernel, 1, NULL, globalWorkSize, LocalWorkSize, 0, NULL, NULL);
	
	unsigned int * debugSplitter = new unsigned int[2*numSplitter];
	V_RETURN_CL(clEnqueueReadBuffer(CommandQueue, splitters, CL_TRUE, 0,  sizeof(cl_uint) * 2*numSplitter, debugSplitter, 0, NULL, NULL), "Error reading data from device!");
	
	std::cout << "Splitter:";
	for (unsigned int i = 0; i < 2*numSplitter; ++i) {
		std::cout << debugSplitter[i] << ", ";
	}
	std::cout << std::endl;
	std::cout << std::endl;
	
	
	
	/*
	 * Merge
	 */
	 
	// __kernel void SplitterMerge(__global uint* inArray, __global uint* outArray, uint size, __global uint* splitters, uint seqSize,  __local uint* localBlockA, __local uint* localBlockB, __local uint* localResult, uint outputIndex)
	
	
	//m_SplitterMergeKernel
	
	cl_mem mergeResult = clCreateBuffer(Context, CL_MEM_READ_WRITE, sizeof(cl_uint) * m_N, NULL, &clErr);
	V_RETURN_CL(clErr, "Error allocating device array mergeResult");
	
	clErr = clSetKernelArg(m_SplitterMergeKernel, 0, sizeof(cl_mem), (void*) &m_dArray);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: array");
	
	clErr = clSetKernelArg(m_SplitterMergeKernel, 1, sizeof(cl_mem), (void*) &mergeResult);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: array");
	
	clErr = clSetKernelArg(m_SplitterMergeKernel, 2, sizeof(cl_uint), (void*) &m_N);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: size");
	
	clErr = clSetKernelArg(m_SplitterMergeKernel, 3, sizeof(cl_mem), (void*) &splitters);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: splitters");
	
	cl_uint seqSize = 16;
	clErr = clSetKernelArg(m_SplitterMergeKernel, 4, sizeof(cl_uint), (void*)&seqSize);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: seqSize");
	
	clErr = clSetKernelArg(m_SplitterMergeKernel, 5, sizeof(cl_uint)*LocalWorkSize[0], NULL);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: localBlockA");
	clErr = clSetKernelArg(m_SplitterMergeKernel, 6, sizeof(cl_uint)*LocalWorkSize[0], NULL);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: localBlockB");
	clErr = clSetKernelArg(m_SplitterMergeKernel, 7, sizeof(cl_uint)*2*LocalWorkSize[0], NULL);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: localBlockResult");
	
	cl_uint indexOutput = 1;
	clErr = clSetKernelArg(m_SplitterMergeKernel, 8, sizeof(cl_uint), (void*)&indexOutput);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: indexOutput");
	
	
	// get global and local work size in dependence of localThreads
	globalWorkSize[0] = GetGlobalWorkSize(m_N, LocalWorkSize[0]);

	// execute the kernel
	clErr = clEnqueueNDRangeKernel(CommandQueue, m_SplitterMergeKernel, 1, NULL, globalWorkSize, LocalWorkSize, 0, NULL, NULL);
	
	unsigned int * outArray = new unsigned int[m_N];
	V_RETURN_CL(clEnqueueReadBuffer(CommandQueue, mergeResult, CL_TRUE, 0,  sizeof(cl_uint) * m_N, outArray, 0, NULL, NULL), "Error reading data from device!");
	
	std::cout << "Merge Output:    ";
	for (unsigned int i = 0; i < m_N; ++i) {
		std::cout << outArray[i] << ", ";
	}
	std::cout << std::endl;
	std::cout << std::endl;
}






/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
