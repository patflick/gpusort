


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__kernel void Scan_Naive(const __global uint* inArray, __global uint* outArray, uint N, uint offset) 
{

	uint right_index = get_global_id(0);
	int left_index = right_index - offset;
	
	if (right_index < N) {
		outArray[right_index] = inArray[right_index];
		if (left_index >= 0) {
			outArray[right_index] += inArray[left_index];
		}
	}
}



// Some usefull defines
#define NUM_BANKS			32
#define NUM_BANKS_LOG		5
#define SIMD_GROUP_SIZE		32

// Bank conflicts
#define AVOID_BANK_CONFLICTS
#ifdef AVOID_BANK_CONFLICTS
	// define your conflict free macro here
	#define OFFSET(A) ((A)+((A)>>NUM_BANKS_LOG)) 
#else
	#define OFFSET(A) (A)
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__kernel void Scan_WorkEfficient(__global uint* array, __global uint* higherLevelArray, __local uint* localBlock) 
{

	uint LID = get_local_id(0);
	uint LS = get_local_size(0);
	uint GID = get_global_id(0);
	uint Goffset = get_group_id(0)*LS*2;
	
	// copy data from global to local array
	// coalescing access by first copying the first half then the second half
	localBlock[OFFSET(LID)] = array[Goffset+LID];
	localBlock[OFFSET(LS+LID)] = array[Goffset+LS+LID];
	
	// barrier waiting for local mem
	barrier(CLK_LOCAL_MEM_FENCE);

	/*
	// first step: Up-sweep
	int threadmapping = (LID*2)+1;
	for (int stride = 1; stride < LS*2; stride *= 2) {
		barrier(CLK_LOCAL_MEM_FENCE);
		if ((threadmapping+1) % (stride*2) == 0) {
			localBlock[OFFSET(threadmapping)] = localBlock[OFFSET(threadmapping-stride)] + localBlock[OFFSET(threadmapping)];
		}
	} 
	*/
	
	
	// more efficient version
	uint threads = LS;
	int stride = 1;
	for ( ; threads > SIMD_GROUP_SIZE; threads >>= 1) {
		if (LID < threads) {
			localBlock[OFFSET(2*LS-1-2*stride*LID)] += localBlock[OFFSET(2*LS-1-stride*(2*LID+1))];
		}
		
		barrier(CLK_LOCAL_MEM_FENCE);
		
		stride <<= 1;
	}
	
	
	// Barrier avoidance:
	if (LID < SIMD_GROUP_SIZE) {
		// first step: Up-sweep
		// last 32 threads without barrier (warp)
		for ( ; threads > 0; threads >>= 1) {	
			if (LID < threads) {
				localBlock[OFFSET(2*LS-1-2*stride*LID)] += localBlock[OFFSET(2*LS-1-stride*(2*LID+1))];
			}
			stride <<= 1;
		}
	
		
		if (LID == 0) {
			// last thread: write into higher level buffer
			higherLevelArray[get_group_id(0)] = localBlock[OFFSET(2*LS-1)];
			localBlock[OFFSET(2*LS-1)] = 0;
		}

		// second step: Down-sweep
		threads = 1;
		stride = LS;
		// barrier avoidance, the first 32 threads are a warp
		for ( ; threads <= SIMD_GROUP_SIZE && threads <= LS; threads <<= 1) {
			if (LID < threads) {
				// first read then write
				uint left_child_value = localBlock[OFFSET(2*LS-1-2*stride*LID-stride)];
				uint right_child_value = localBlock[OFFSET(2*LS-1-2*stride*LID)];
				localBlock[OFFSET(2*LS-1-2*stride*LID-stride)] = right_child_value;
				localBlock[OFFSET(2*LS-1-2*stride*LID)] = left_child_value + right_child_value;
			}
			stride >>= 1;
		}
	}
	else
	{
		threads <<= 1;
		stride >>= 1;
	}
	
	for ( ; threads <= LS; threads <<= 1) {
		barrier(CLK_LOCAL_MEM_FENCE);
		if (LID < threads) {
			// first read then write
			uint left_child_value = localBlock[OFFSET(2*LS-1-2*stride*LID-stride)];
			uint right_child_value = localBlock[OFFSET(2*LS-1-2*stride*LID)];
			localBlock[OFFSET(2*LS-1-2*stride*LID-stride)] = right_child_value;
			localBlock[OFFSET(2*LS-1-2*stride*LID)] = left_child_value + right_child_value;
		}
		stride >>= 1;
	}
	
	/*
	for (int stride = LS; stride > 0; stride /= 2) {
		barrier(CLK_LOCAL_MEM_FENCE);
		if ((threadmapping+1) % (stride*2) == 0) {
			// first read then write
			uint left_child_value = localBlock[OFFSET(threadmapping-stride)];
			uint right_child_value = localBlock[OFFSET(threadmapping)];
			localBlock[OFFSET(threadmapping-stride)] = right_child_value;
			localBlock[OFFSET(threadmapping)] = left_child_value + right_child_value;
		}
	}
	
	*/


	// barrier
	barrier(CLK_LOCAL_MEM_FENCE);
	
	// add local results to global array
	array[Goffset+LID] += localBlock[OFFSET(LID)];
	array[Goffset+LS+LID] += localBlock[OFFSET(LS+LID)];
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Kernel that should add the group PPS to the local PPS (Figure 14)
__kernel void Scan_WorkEfficientAdd(__global uint* higherLevelArray, __global uint* array, __local uint* localBlock) 
{
  	uint LID = get_local_id(0);
	uint GID = get_global_id(0);
	uint LS = get_local_size(0);
	uint Goffset = get_group_id(0)*LS*2;
	
	// every thread writes two!
	if (get_group_id(0) > 0) {
		array[Goffset+LID] += higherLevelArray[get_group_id(0)-1];
		array[Goffset+LS+LID] += higherLevelArray[get_group_id(0)-1];
	}

}
