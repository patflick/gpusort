/*
*/

#ifndef CRadixSort_H
#define CRadixSort_H

#include "IAssignment.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class CRadixSort
	:public IAssignment
{
protected:
	//NOTE: we have two memory address spaces, so we mark pointers with a prefix
	//to avoid confusions: 'h' - host, 'd' - device

	unsigned int		m_N;

	// input data
	unsigned int		*m_hInput;
	// results
	unsigned int		*m_resultCPU;
	unsigned int		*m_resultGPU;

	cl_mem				m_dArray;

	//OpenCL program and kernels
	cl_program			m_Program;
	cl_kernel			m_ScanNaiveKernel;
	cl_kernel			m_GetBitFlagsKernel;
	cl_kernel			m_AddLastElementKernel;
	cl_kernel			m_PermutePSKernel;
	cl_kernel			m_LocalSplitRadixSortKernel;
	cl_kernel			m_HistrogramShuffleKernel;
	cl_kernel			m_ScanWorkEfficientKernel;
	cl_kernel			m_ScanWorkEfficientAddKernel;

	void ValidateTask(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3], unsigned int task);
	

public:
	CRadixSort(unsigned int N);

	virtual ~CRadixSort();
	
	void TestPerformance(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3], unsigned int task);
	
	virtual bool InitResources(cl_device_id Device, cl_context Context);
	
	virtual void ReleaseResources();

	virtual void ComputeGPU(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3]);

	virtual void ComputeCPU();

	virtual bool ValidateResults();
	
	/* ************************
	 *  Slow/Naive RadixSort
	 * ************************/
	
	// test functions:
	void TestFlags(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3]);
	void TestPrefixSums(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3]);
	void TestPermutation(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3]);
	
	// actual sort:
	void SplitRadixSort(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3]);
	
	// basic functions
	void PermutePS(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3],unsigned int size, cl_mem& inArray, cl_mem& outArray, cl_mem& zeroFlagsPS, cl_mem& oneFlagsPS, unsigned int bit);
	void GenerateFlags(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3], unsigned int size, cl_mem& inArray, cl_mem& flags, unsigned int bit, bool bitOne);
	void GetFlagPrefixSums(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3],unsigned int size, cl_mem& inArray, cl_mem& zeroFlagsPS, cl_mem& oneFlagsPS, unsigned int bit);
	void AddLastElement(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3], unsigned int size, cl_mem& sum, cl_mem& inArray);
	
	// Prefix Sum:
	void Scan_Naive(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3], unsigned int size, cl_mem& array);
	void Scan_WorkEfficient(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3], unsigned int size, cl_mem& dArray);
	
	/* ************************
	 *    Efficient RadixSort
	 * ************************/
	 
	// test functions:
	void TestLocalRadixSort(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3]);
	
	// basic functions:
	void LocalSplitRadixSort(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3],unsigned int size, cl_mem& inArray, cl_mem& histograms, unsigned int startbit, unsigned int b);
	void HistrogramShuffle(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3], cl_mem& inArray, cl_mem& outArray, cl_mem& histogramPS, unsigned int size, unsigned int startbit, unsigned int b);
	
	// actual sorting routine:
	void EfficientRadixSort(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3]);
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#endif
