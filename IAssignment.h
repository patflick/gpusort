/*
Common interface for the assignments.
*/

#ifndef IASSIGNMENT_H
#define IASSIGNMENT_H

#include "Common.h"

class IAssignment
{
public:

	//Init any resources specific to the current task
	virtual bool InitResources(cl_device_id Device, cl_context Context) = 0;

	//release everything allocated in InitResources()
	virtual void ReleaseResources() = 0;

	//Perform calculations on the GPU
	virtual void ComputeGPU(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3]) = 0;

	// perform performance tests
	virtual void TestPerformance(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3], unsigned int task) = 0;

	//Compute the "golden" solution on the CPU. The GPU results must be equal to this reference
	virtual void ComputeCPU() = 0;

	//compare the GPU solution to the "golden" solution
	virtual bool ValidateResults() = 0;
};

#endif
