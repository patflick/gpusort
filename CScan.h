/*
*/

#ifndef CSCAN_H
#define CSCAN_H

#include "IAssignment.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class CScan
	:public IAssignment
{
protected:
	//NOTE: we have two memory address spaces, so we mark pointers with a prefix
	//to avoid confusions: 'h' - host, 'd' - device

	unsigned int		m_N;

	//float data on the CPU
	unsigned int		*m_hArray;

	unsigned int		*m_hResultCPU;
	unsigned int		*m_hResultGPU;

	// ping-pong arrays for the naive scan
	cl_mem				m_dPingArray;
	cl_mem				m_dPongArray;

	// arrays for each level of the work-efficient scan
	unsigned int		m_nLevels;
	cl_mem				*m_dLevelArrays;

	//OpenCL program and kernels
	cl_program			m_Program;
	cl_kernel			m_ScanNaiveKernel;
	cl_kernel			m_ScanWorkEfficientKernel;
	cl_kernel			m_ScanWorkEfficientAddKernel;

	size_t				m_LocalWorkSize[3];

	void Scan_Naive(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3]);
	void Scan_WorkEfficient(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3]);

	void ValidateTask(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3], unsigned int task);
	void TestPerformance(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3], unsigned int task);

public:
	CScan(unsigned int N, size_t LocalWorkSize[3]);

	virtual ~CScan();

	
	virtual bool InitResources(cl_device_id Device, cl_context Context);
	
	virtual void ReleaseResources();

	virtual void ComputeGPU(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3]);

	virtual void ComputeCPU();

	virtual bool ValidateResults();
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#endif