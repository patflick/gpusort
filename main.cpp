// All OpenCL headers
#if defined (__APPLE__) || defined(MACOSX)
    #include <OpenCL/opencl.h>
#else
    #include <CL/opencl.h>
#endif 

#include "Common.h"
#include "CSort.h"
#include "CRadixSort.h"

using namespace std;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Global variables and forward declarations

cl_context		g_CLContext		= NULL;
cl_command_queue	g_CLCommandQueue	= NULL;
cl_platform_id		g_CLPlatform		= NULL;
cl_device_id		g_CLDevice		= NULL;

bool InitContextResources();
void CleanupContextResources();
bool RunAssigment(IAssignment& Assignment, size_t LocalWorkSize[3]);
bool RunBenchmarks();

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv)
{
	if(InitContextResources())
	{
		cout<<"OpenCL context initialized."<<endl;

		/*
		cout<<"########################################"<<endl;
		cout<<"TEST SORT 3: Local Multiple EvenOddMergesort"<<endl<<endl;
		{
			size_t LocalWorkSize[3] = {256, 1, 1};
			CSort sort(1<<22);

			//CReduction reduction(32*1024);
			RunAssigment(sort, LocalWorkSize);
		}
		*/
		
		/*
		cout<<"########################################"<<endl;
		cout<<"TEST RADIX SORT 0: Flags"<<endl<<endl;
		{
			size_t LocalWorkSize[3] = {256, 1, 1};
			CRadixSort sort(1<<20);

			//CReduction reduction(32*1024);
			RunAssigment(sort, LocalWorkSize);
		}
		*/
		
		RunBenchmarks();
	}

	CleanupContextResources();
	
	cout<<"Press any key..."<<endl;
//	cin.get();
}

bool InitContextResources()
{
	//error code
	cl_int clError;
	
	//get platform ID
	V_RETURN_FALSE_CL( clGetPlatformIDs(1, &g_CLPlatform, NULL), "Failed to get CL platform ID" );

	//get a reference to the first available GPU device
	V_RETURN_FALSE_CL( clGetDeviceIDs(g_CLPlatform, CL_DEVICE_TYPE_GPU, 1, &g_CLDevice, NULL), "No GPU device found." );

	char deviceName[256];
	V_RETURN_FALSE_CL( clGetDeviceInfo(g_CLDevice, CL_DEVICE_NAME, 256, &deviceName, NULL), "Unable to query device name.");
	cout << "Device: " << deviceName << endl;

	//Create a new OpenCL context on the selected device
	g_CLContext = clCreateContext(0, 1, &g_CLDevice, NULL, NULL, &clError);
	V_RETURN_FALSE_CL(clError, "Failed to create OpenCL context.");

	//Finally, create the command queue. All the asynchronous commands to the device will be issued
	//from the CPU into this queue. This way the host program can continue the execution until some results
	//from that device are needed.
	g_CLCommandQueue = clCreateCommandQueue(g_CLContext, g_CLDevice, 0, &clError);
	V_RETURN_FALSE_CL(clError, "Failed to create the command queue in the context");

	return true;
}

void CleanupContextResources()
{
	if(g_CLCommandQueue)	clReleaseCommandQueue(g_CLCommandQueue);
	if(g_CLContext)			clReleaseContext(g_CLContext);
}

bool RunAssigment(IAssignment& pAssignment, size_t LocalWorkSize[3])
{
	if(!pAssignment.InitResources(g_CLDevice, g_CLContext))
	{
		cout<<"Error during resource allocation. Aborting execution."<<endl;
		pAssignment.ReleaseResources();
		return false;
	}

	//compute golden result
	cout<<"Computing CPU reference result..."<<endl;
	pAssignment.ComputeCPU();

	//running the same task on the GPU
	cout<<endl<<"Computing GPU result..."<<endl;
	
	//run  the kernel.
	pAssignment.ComputeGPU(g_CLContext, g_CLCommandQueue, LocalWorkSize);

	//cleanup
	pAssignment.ReleaseResources();

	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool RunBenchmarks() {
	

	
	unsigned int localWorkSize = 256;
	unsigned int maxN = 1<<23;
	
	std::cout << "N;Time.CPU;Time.NaiveRadixSort;Time.EfficientRadixSort;Time.BinaryMergeSort;Time.EfficientMergeSort" << std::endl;
	
	for (unsigned int N = localWorkSize; N <= maxN; N = N<<1) {
		size_t LocalWorkSize[3] = {256, 1, 1};
		CRadixSort radixsort(N);
		CSort mergeSort(N);
		
		if(!radixsort.InitResources(g_CLDevice, g_CLContext))
		{
			cout<<"Error during resource allocation. Aborting execution."<<endl;
			radixsort.ReleaseResources();
			return false;
		}
		if(!mergeSort.InitResources(g_CLDevice, g_CLContext))
		{
			cout<<"Error during resource allocation. Aborting execution."<<endl;
			mergeSort.ReleaseResources();
			return false;
		}
		
		std::cout << N << ";";
		
		radixsort.ComputeCPU();
		std::cout << ";";
		std::cout.flush();
		
		radixsort.TestPerformance(g_CLContext, g_CLCommandQueue, LocalWorkSize, 1);
		std::cout << ";";
		std::cout.flush();
		radixsort.TestPerformance(g_CLContext, g_CLCommandQueue, LocalWorkSize, 2);
		std::cout << ";";
		std::cout.flush();
		mergeSort.TestPerformance(g_CLContext, g_CLCommandQueue, LocalWorkSize, 2);
		std::cout << ";";
		std::cout.flush();
		mergeSort.TestPerformance(g_CLContext, g_CLCommandQueue, LocalWorkSize, 1);
		
		std::cout << std::endl;
	}
	
	return true;
}

