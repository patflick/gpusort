CC=g++
CFLAGS=-c -Wall -O2
# library path for ATIS machines:
LDFLAGS=-L/usr/lib64/nvidia -lOpenCL
# include path for ATIS machines:
INCLUDE=-I/opt/NVIDIA_GPU_Computing_SDK/OpenCL/common/inc
# include path for my machine
INCLUDE=-I/usr/local/cuda/include
SOURCES=main.cpp Common.cpp CTimer.cpp CSort.cpp CRadixSort.cpp
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=gpusort

all: $(SOURCES) $(EXECUTABLE)
	
$(EXECUTABLE): $(OBJECTS)
	$(CC) $(OBJECTS) -o $@ $(LDFLAGS)

.cpp.o:
	$(CC) $(CFLAGS) $(INCLUDE) $< -o $@
	
clean:
	rm -fv *.o
	rm -fv $(EXECUTABLE)
