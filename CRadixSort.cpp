#include "CRadixSort.h"
#include "CTimer.h"
#include "Common.h"
#include <algorithm>
#include <time.h>

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CSort

CRadixSort::CRadixSort(unsigned int N) 
	: m_N(N), m_hInput(NULL), 
	m_dArray(NULL),
	m_Program(NULL), 
	m_ScanNaiveKernel(NULL),
	m_GetBitFlagsKernel(NULL),
	m_AddLastElementKernel(NULL),
	m_PermutePSKernel(NULL),
	m_LocalSplitRadixSortKernel(NULL),
	m_HistrogramShuffleKernel(NULL),
	m_ScanWorkEfficientKernel(NULL),
	m_ScanWorkEfficientAddKernel(NULL)
{
}

CRadixSort::~CRadixSort() 
{
}

bool CRadixSort::InitResources(cl_device_id Device, cl_context Context) {
	//CPU resources
	m_hInput = new unsigned int[m_N];
	m_resultCPU = new unsigned int[m_N];
	m_resultGPU = new unsigned int[m_N];

	srand ( time(NULL) );
	//srand(1);
	//fill the array with some values
	for(unsigned int i = 0; i < m_N; i++) 
		//m_hInput[i] = 1;			// Use this for debugging
		m_hInput[i] = rand() & 15;
		//m_hInput[i] = rand();
		



	/*
	std::cout << "Input: ";
	for (unsigned int i = 0; i < m_N; i++) {
		std::cout << m_hInput[i] << ", ";
	}
	std::cout << std::endl;
	std::cout << std::endl;
	*/
	
	
	cl_int clError;
	
	
	//device resources
	m_dArray = clCreateBuffer(Context, CL_MEM_READ_WRITE, sizeof(cl_uint) * m_N, NULL, &clError);
	V_RETURN_FALSE_CL(clError, "Error allocating device arrays");
	

	

	//load and compile kernels
	char* programCode = NULL;
	size_t programSize = 0;

	LoadProgram("RadixSort.cl", &programCode, &programSize);

	//create program object
	m_Program = clCreateProgramWithSource(Context, 1, (const char**) &programCode, &programSize, &clError);
	V_RETURN_FALSE_CL(clError, "Failed to create program from file.");

	//build program
	clError = clBuildProgram(m_Program, 1, &Device, NULL, NULL, NULL);
	if(clError != CL_SUCCESS) {
		PrintBuildLog(m_Program, Device);
		return false;
	}

	//create kernels
	m_ScanNaiveKernel = clCreateKernel(m_Program, "Scan_Naive", &clError);
	V_RETURN_FALSE_CL(clError, "Failed to create kernel.");
	
	m_ScanWorkEfficientKernel = clCreateKernel(m_Program, "Scan_WorkEfficient", &clError);
	V_RETURN_FALSE_CL(clError, "Failed to create kernel.");

	m_ScanWorkEfficientAddKernel = clCreateKernel(m_Program, "Scan_WorkEfficientAdd", &clError);
	V_RETURN_FALSE_CL(clError, "Failed to create kernel.");
	
	m_GetBitFlagsKernel = clCreateKernel(m_Program, "GetBitFlags", &clError);
	V_RETURN_FALSE_CL(clError, "Failed to create kernel.");
	
	m_AddLastElementKernel = clCreateKernel(m_Program, "AddLastElement", &clError);
	V_RETURN_FALSE_CL(clError, "Failed to create kernel.");
	
	m_PermutePSKernel = clCreateKernel(m_Program, "PermutePS", &clError);
	V_RETURN_FALSE_CL(clError, "Failed to create kernel.");
	
	m_LocalSplitRadixSortKernel = clCreateKernel(m_Program, "LocalSplitRadixSort", &clError);
	V_RETURN_FALSE_CL(clError, "Failed to create kernel.");
	
	m_HistrogramShuffleKernel = clCreateKernel(m_Program, "HistrogramShuffle", &clError);
	V_RETURN_FALSE_CL(clError, "Failed to create kernel.");
	
	return true;
}

void CRadixSort::ReleaseResources() {
	// host resources
	SAFE_DELETE_ARRAY(m_hInput);
	SAFE_DELETE_ARRAY(m_resultCPU);
	SAFE_DELETE_ARRAY(m_resultGPU);

	// device resources
	SAFE_RELEASE_MEMOBJECT(m_dArray);

	SAFE_RELEASE_KERNEL(m_ScanNaiveKernel);
	SAFE_RELEASE_KERNEL(m_ScanWorkEfficientKernel);
	SAFE_RELEASE_KERNEL(m_ScanWorkEfficientAddKernel);
	SAFE_RELEASE_KERNEL(m_GetBitFlagsKernel);
	SAFE_RELEASE_KERNEL(m_AddLastElementKernel);
	SAFE_RELEASE_KERNEL(m_PermutePSKernel);
	SAFE_RELEASE_KERNEL(m_LocalSplitRadixSortKernel);
	SAFE_RELEASE_KERNEL(m_HistrogramShuffleKernel);
	
	
	SAFE_RELEASE_PROGRAM(m_Program);
}

void CRadixSort::ComputeCPU() {

	CTimer timer;
	timer.Start();

	unsigned int nIterations = 10;
	for(unsigned int j = 0; j < nIterations; j++) {
		// copy data
		for(unsigned int i = 0; i < m_N; i++) 
			m_resultCPU[i] = m_hInput[i];
		std::sort(m_resultCPU, m_resultCPU+m_N);
	}

	timer.Stop();
	double ms = 1000 * timer.GetElapsedTime() / double(nIterations);
	//cout << "  average time: " << ms << " ms, throughput: " << 1.0e-6 * (double)m_N / ms << " Gelem/s" <<endl;
	cout << ms;

}

bool CRadixSort::ValidateResults() {
	  if(memcmp(m_resultCPU, m_resultGPU, m_N * sizeof(unsigned int)) == 0) {
		  return true;
	  }
	  std::cout << "Gotten Result: ";
	  for (unsigned int i = 0; i < m_N; i++) {
		std::cout << m_resultGPU[i] << ", ";
	  }
	  std::cout << std::endl;
	  std::cout << "Expected Result: ";
	  for (unsigned int i = 0; i < m_N; i++) {
		std::cout << m_resultCPU[i] << ", ";
	  }
	  std::cout << std::endl;
	  return false;
}



void CRadixSort::ValidateTask(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3], unsigned int task){

	cout << "Validating results of task " << task << endl;

	//write input data to the GPU
	V_RETURN_CL(clEnqueueWriteBuffer(CommandQueue, m_dArray, CL_FALSE, 0, m_N * sizeof(cl_uint), m_hInput, 0, NULL, NULL), "Error copying data from host to device!");
	
	// TODO remove this:
	std::cout << "localworksize: " << LocalWorkSize[0] << std::endl;
	
	//run selected task
	switch (task){
		case 1:
			//TestFlags(Context, CommandQueue, LocalWorkSize);
			
			//TestPrefixSums(Context, CommandQueue, LocalWorkSize);
			
			//TestPermutation(Context, CommandQueue, LocalWorkSize);
			
			SplitRadixSort(Context, CommandQueue, LocalWorkSize);
			
			break;
		case 2:
			//TestLocalRadixSort(Context, CommandQueue, LocalWorkSize);
			EfficientRadixSort(Context, CommandQueue, LocalWorkSize);
			break;
		case 3:

			break;
	}

	//read back the results synchronously.
	V_RETURN_CL(clEnqueueReadBuffer(CommandQueue, m_dArray, CL_TRUE, 0, m_N * sizeof(cl_uint), m_resultGPU, 0, NULL, NULL), "Error reading data from device!");

	if(ValidateResults())
		cout<<"  TEST PASSED!"<<endl;
	else
		cout<<"  INVALID RESULTS!"<<endl;
}


void CRadixSort::TestPerformance(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3], unsigned int task){

	//cout << "Testing performance of task " << task << endl;

	//write input data to the GPU
	V_RETURN_CL(clEnqueueWriteBuffer(CommandQueue, m_dArray, CL_FALSE, 0, m_N * sizeof(cl_uint), m_hInput, 0, NULL, NULL), "Error copying data from host to device!");
	//finish all before we start meassuring the time
	V_RETURN_CL(clFinish(CommandQueue), "Error finishing the queue!");

	CTimer timer;
	timer.Start();

	//run the kernel N times
	unsigned int nIterations = 10;
	for(unsigned int i = 0; i < nIterations; i++) {
		//run selected task
		switch (task){
			case 1:
				SplitRadixSort(Context, CommandQueue, LocalWorkSize);
				break;
			case 2:
				EfficientRadixSort(Context, CommandQueue, LocalWorkSize);
		}
	}

	//wait until the command queue is empty again
	V_RETURN_CL(clFinish(CommandQueue), "Error finishing the queue!");

	timer.Stop();

	double ms = 1000 * timer.GetElapsedTime() / double(nIterations);
	//cout << "  average time: " << ms << " ms, throughput: " << 1.0e-6 * (double)m_N / ms << " Gelem/s" <<endl;
	cout << ms;
}



void CRadixSort::ComputeGPU(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3]){

	cout << endl;

	ValidateTask(Context, CommandQueue, LocalWorkSize, 2);

	cout << endl;

	//TestPerformance(Context, CommandQueue, LocalWorkSize, 1);
	TestPerformance(Context, CommandQueue, LocalWorkSize, 2);

	cout << endl;
}

void CRadixSort::TestLocalRadixSort(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3]) {
	unsigned int b = 2;
	unsigned histSize = (1 << b)*(GetGlobalWorkSize(m_N, LocalWorkSize[0]) / LocalWorkSize[0]);
	
	// create output histrograms
	cl_int clErr;
	cl_mem dHistrograms = clCreateBuffer(Context, CL_MEM_READ_WRITE, sizeof(cl_uint) * histSize, NULL, &clErr);
	V_RETURN_CL(clErr, "Error allocating device array dHistrograms");
	cl_mem dOutput = clCreateBuffer(Context, CL_MEM_READ_WRITE, sizeof(cl_uint) * m_N, NULL, &clErr);
	V_RETURN_CL(clErr, "Error allocating device array dHistrograms");
	
	LocalSplitRadixSort(Context, CommandQueue, LocalWorkSize, m_N, m_dArray, dHistrograms,  0, b);
	
	// output histrogram:
	unsigned int * hist = new unsigned int[histSize];
	V_RETURN_CL(clEnqueueReadBuffer(CommandQueue, dHistrograms, CL_TRUE, 0,  sizeof(cl_uint) * histSize, hist, 0, NULL, NULL), "Error reading data from device!");
	
	std::cout << "Histrogram:  ";
	for (unsigned int i = 0; i < histSize; ++i) {
		std::cout << hist[i] << ", ";
	}
	std::cout << std::endl;
	std::cout << std::endl;
	
	Scan_Naive(Context, CommandQueue, LocalWorkSize, histSize, dHistrograms);
	
	HistrogramShuffle(Context, CommandQueue, LocalWorkSize, m_dArray, dOutput, dHistrograms, m_N, 0, b);
	
	// output result:
	unsigned int * outputArray = new unsigned int[m_N];
	V_RETURN_CL(clEnqueueReadBuffer(CommandQueue, m_dArray, CL_TRUE, 0,  sizeof(cl_uint) * m_N, outputArray, 0, NULL, NULL), "Error reading data from device!");
	
	std::cout << "Local Output:  ";
	for (unsigned int i = 0; i < m_N; ++i) {
		std::cout << outputArray[i] << ", ";
	}
	std::cout << std::endl;
	std::cout << std::endl;
	
	// output histrogram:
	V_RETURN_CL(clEnqueueReadBuffer(CommandQueue, dHistrograms, CL_TRUE, 0,  sizeof(cl_uint) * histSize, hist, 0, NULL, NULL), "Error reading data from device!");
	
	std::cout << "Histrogram:  ";
	for (unsigned int i = 0; i < histSize; ++i) {
		std::cout << hist[i] << ", ";
	}
	std::cout << std::endl;
	std::cout << std::endl;
	
	// output shuffled:
	unsigned int * shuffled = new unsigned int[m_N];
	V_RETURN_CL(clEnqueueReadBuffer(CommandQueue, dOutput, CL_TRUE, 0,  sizeof(cl_uint) * m_N, shuffled, 0, NULL, NULL), "Error reading data from device!");
	  
	std::cout << "Shuffled:        ";
	for (unsigned int i = 0; i < m_N; ++i) {
		std::cout << shuffled[i] << ", ";
	}
	std::cout << std::endl;
	std::cout << std::endl;
}


void CRadixSort::TestPrefixSums(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3]) {
	// create output arrays
	cl_int clErr;
	cl_mem dZeroPS = clCreateBuffer(Context, CL_MEM_READ_WRITE, sizeof(cl_uint) * m_N, NULL, &clErr);
	V_RETURN_CL(clErr, "Error allocating device array dZeroPS");
	cl_mem dOnePS = clCreateBuffer(Context, CL_MEM_READ_WRITE, sizeof(cl_uint) * m_N, NULL, &clErr);
	V_RETURN_CL(clErr, "Error allocating device array dOnePS");
	
	GetFlagPrefixSums(Context, CommandQueue, LocalWorkSize, m_N, m_dArray, dZeroPS, dOnePS, 0);
	
	// output both prefix sums:
	unsigned int * zeroPS = new unsigned int[m_N];
	V_RETURN_CL(clEnqueueReadBuffer(CommandQueue, dZeroPS, CL_TRUE, 0,  sizeof(cl_uint) * m_N, zeroPS, 0, NULL, NULL), "Error reading data from device!");
	
	std::cout << "ZeroPS Output:  ";
	for (unsigned int i = 0; i < m_N; ++i) {
		std::cout << zeroPS[i] << ", ";
	}
	std::cout << std::endl;
	std::cout << std::endl;
	
	unsigned int * onePS = new unsigned int[m_N];
	V_RETURN_CL(clEnqueueReadBuffer(CommandQueue, dOnePS, CL_TRUE, 0,  sizeof(cl_uint) * m_N, onePS, 0, NULL, NULL), "Error reading data from device!");
	
	std::cout << "OnePS Output:   ";
	for (unsigned int i = 0; i < m_N; ++i) {
		std::cout << onePS[i] << ", ";
	}
	std::cout << std::endl;
	std::cout << std::endl;
}


void CRadixSort::TestPermutation(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3]) {
	// create output arrays
	cl_int clErr;
	cl_mem dZeroPS = clCreateBuffer(Context, CL_MEM_READ_WRITE, sizeof(cl_uint) * m_N, NULL, &clErr);
	V_RETURN_CL(clErr, "Error allocating device array dZeroPS");
	cl_mem dOnePS = clCreateBuffer(Context, CL_MEM_READ_WRITE, sizeof(cl_uint) * m_N, NULL, &clErr);
	V_RETURN_CL(clErr, "Error allocating device array dOnePS");
	cl_mem dOutput = clCreateBuffer(Context, CL_MEM_READ_WRITE, sizeof(cl_uint) * m_N, NULL, &clErr);
	V_RETURN_CL(clErr, "Error allocating device array dOutput");
	
	GetFlagPrefixSums(Context, CommandQueue, LocalWorkSize, m_N, m_dArray, dZeroPS, dOnePS, 0);
	PermutePS(Context, CommandQueue, LocalWorkSize, m_N, m_dArray, dOutput, dZeroPS, dOnePS, 0);
	
	
	// output result:
	unsigned int * outputArray = new unsigned int[m_N];
	V_RETURN_CL(clEnqueueReadBuffer(CommandQueue, dOutput, CL_TRUE, 0,  sizeof(cl_uint) * m_N, outputArray, 0, NULL, NULL), "Error reading data from device!");
	
	std::cout << "Permu Output:  ";
	for (unsigned int i = 0; i < m_N; ++i) {
		std::cout << outputArray[i] << ", ";
	}
	std::cout << std::endl;
	std::cout << std::endl;
}



void CRadixSort::TestFlags(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3]) {
	// create output array
	cl_int clErr;
	cl_mem dOutputArray = clCreateBuffer(Context, CL_MEM_READ_WRITE, sizeof(cl_uint) * m_N, NULL, &clErr);
	V_RETURN_CL(clErr, "Error allocating device array outputArray");
	
	GenerateFlags(Context, CommandQueue, LocalWorkSize, m_N, m_dArray, dOutputArray, 0, false);
	
	unsigned int * outArray = new unsigned int[m_N];
	V_RETURN_CL(clEnqueueReadBuffer(CommandQueue, dOutputArray, CL_TRUE, 0,  sizeof(cl_uint) * m_N, outArray, 0, NULL, NULL), "Error reading data from device!");
	
	std::cout << "Flags Output:   ";
	for (unsigned int i = 0; i < m_N; ++i) {
		std::cout << outArray[i] << ", ";
	}
	std::cout << std::endl;
	std::cout << std::endl;
	
	// run a scan 
	
	Scan_Naive(Context, CommandQueue, LocalWorkSize, m_N, dOutputArray);
	
	V_RETURN_CL(clEnqueueReadBuffer(CommandQueue, dOutputArray, CL_TRUE, 0,  sizeof(cl_uint) * m_N, outArray, 0, NULL, NULL), "Error reading data from device!");
	
	std::cout << "Scan Output:    ";
	for (unsigned int i = 0; i < m_N; ++i) {
		std::cout << outArray[i] << ", ";
	}
	std::cout << std::endl;
	std::cout << std::endl;
}

void CRadixSort::EfficientRadixSort(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3]) {
	// TODO: Tuning parameter b!
	int b = 2;
	unsigned histSize = (1 << b)*(GetGlobalWorkSize(m_N, LocalWorkSize[0]) / LocalWorkSize[0]);
	
	// create output arrays
	cl_int clErr;
	cl_mem dPongArray = clCreateBuffer(Context, CL_MEM_READ_WRITE, sizeof(cl_uint) * m_N, NULL, &clErr);
	V_RETURN_CL(clErr, "Error allocating device array dOutput");
	cl_mem dHistrograms = clCreateBuffer(Context, CL_MEM_READ_WRITE, sizeof(cl_uint) * histSize, NULL, &clErr);
	V_RETURN_CL(clErr, "Error allocating device array dHistrograms");
	

	// Radix sort from lowest bits to highest bits, by 'b' bits each
	for (unsigned int bit = 0; bit < 32; bit+=b) {
		LocalSplitRadixSort(Context, CommandQueue, LocalWorkSize, m_N, m_dArray, dHistrograms, bit, b);
		// TODO efficient scan!
		//Scan_Naive(Context, CommandQueue, LocalWorkSize, histSize, dHistrograms);
		Scan_WorkEfficient(Context, CommandQueue, LocalWorkSize, histSize, dHistrograms);
		HistrogramShuffle(Context, CommandQueue, LocalWorkSize, m_dArray, dPongArray, dHistrograms, m_N, bit, b);
		
		// switch arrays
		cl_mem tmp = m_dArray;
		m_dArray = dPongArray;
		dPongArray = tmp;
	}
	
	SAFE_RELEASE_MEMOBJECT(dHistrograms);
	SAFE_RELEASE_MEMOBJECT(dPongArray);
}

void CRadixSort::SplitRadixSort(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3]) {
	// create output arrays
	cl_int clErr;
	cl_mem dZeroPS = clCreateBuffer(Context, CL_MEM_READ_WRITE, sizeof(cl_uint) * m_N, NULL, &clErr);
	V_RETURN_CL(clErr, "Error allocating device array dZeroPS");
	cl_mem dOnePS = clCreateBuffer(Context, CL_MEM_READ_WRITE, sizeof(cl_uint) * m_N, NULL, &clErr);
	V_RETURN_CL(clErr, "Error allocating device array dOnePS");
	cl_mem dPongArray = clCreateBuffer(Context, CL_MEM_READ_WRITE, sizeof(cl_uint) * m_N, NULL, &clErr);
	V_RETURN_CL(clErr, "Error allocating device array dOutput");
	
	for (unsigned int bit = 0; bit < 32; bit++) {
		GetFlagPrefixSums(Context, CommandQueue, LocalWorkSize, m_N, m_dArray, dZeroPS, dOnePS, bit);
		PermutePS(Context, CommandQueue, LocalWorkSize, m_N, m_dArray, dPongArray, dZeroPS, dOnePS, bit);
		
		// switch arrays
		cl_mem tmp = m_dArray;
		m_dArray = dPongArray;
		dPongArray = tmp;
	}
	
	SAFE_RELEASE_MEMOBJECT(dZeroPS);
	SAFE_RELEASE_MEMOBJECT(dOnePS);
	SAFE_RELEASE_MEMOBJECT(dPongArray);
}


void CRadixSort::HistrogramShuffle(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3], cl_mem& inArray, cl_mem& outArray, cl_mem& histogramPS, unsigned int size, unsigned int startbit, unsigned int b) {
	
	// arguments:(__global uint* inArray, __global uint* outArray, __global uint* localHistrogramsPS, __local uint* localBuckets, uint N, uint startbit, uint b)
	
	cl_int clErr;
	size_t globalWorkSize[1];
	size_t localWorkSize[1];
	
	// check for the assertion: 2^b <= localsize
	if (LocalWorkSize[0] < (1 << b))
		V_RETURN_CL(1, "Error, histrogram bins dont fit into local size");
	
	// get local and global work size (they dont change)
	localWorkSize[0] = LocalWorkSize[0];
	globalWorkSize[0] = GetGlobalWorkSize(size, localWorkSize[0]);
	size_t localBlockSize = sizeof(cl_uint)*localWorkSize[0];
	
	// set kernel arguments
	clErr = clSetKernelArg(m_HistrogramShuffleKernel, 0, sizeof(cl_mem), (void*) &inArray);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: inArray");
	clErr = clSetKernelArg(m_HistrogramShuffleKernel, 1, sizeof(cl_mem), (void*) &outArray);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: outArray");
	clErr = clSetKernelArg(m_HistrogramShuffleKernel, 2, sizeof(cl_mem), (void*) &histogramPS);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: histogramPS");
	
	// local blocks
	clErr = clSetKernelArg(m_HistrogramShuffleKernel, 3, localBlockSize, NULL);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: local memory block");

	
	// further arguments
	clErr = clSetKernelArg(m_HistrogramShuffleKernel, 4, sizeof(cl_uint), (void*) &size);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: N");
	clErr = clSetKernelArg(m_HistrogramShuffleKernel, 5, sizeof(cl_uint), (void*) &startbit);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: startbit");
	clErr = clSetKernelArg(m_HistrogramShuffleKernel, 6, sizeof(cl_uint), (void*) &b);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: b");
	
	// execute kernel
	clErr = clEnqueueNDRangeKernel(CommandQueue, m_HistrogramShuffleKernel, 1, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
	V_RETURN_CL(clErr, "Error enqueing kernel");
}


void CRadixSort::LocalSplitRadixSort(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3],unsigned int size, cl_mem& inArray, cl_mem& histograms, unsigned int startbit, unsigned int b) {
	
	// arguments:(__global uint* array, __local uint* localBlock1, __local uint* localBlock2, __local uint* localZeroPS, __local uint* localOnePS, uint N, uint startbit, uint b) {
	
	cl_int clErr;
	size_t globalWorkSize[1];
	size_t localWorkSize[1];
	
	// check for the assertion: 2^b <= localsize
	if (LocalWorkSize[0] < (1 << b))
		V_RETURN_CL(1, "Error, histrogram bins dont fit into local size");
	
	// get local and global work size (they dont change)
	localWorkSize[0] = LocalWorkSize[0];
	globalWorkSize[0] = GetGlobalWorkSize(size, localWorkSize[0]);
	size_t localBlockSize = sizeof(cl_uint)*localWorkSize[0];
	
	// set kernel arguments
	clErr = clSetKernelArg(m_LocalSplitRadixSortKernel, 0, sizeof(cl_mem), (void*) &inArray);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: inArray");
	clErr = clSetKernelArg(m_LocalSplitRadixSortKernel, 1, sizeof(cl_mem), (void*) &histograms);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: histograms");
	
	// local blocks
	clErr = clSetKernelArg(m_LocalSplitRadixSortKernel, 2, localBlockSize, NULL);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: local memory block");
	clErr = clSetKernelArg(m_LocalSplitRadixSortKernel, 3, localBlockSize, NULL);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: local memory block");
	clErr = clSetKernelArg(m_LocalSplitRadixSortKernel, 4, localBlockSize, NULL);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: local memory block");
	clErr = clSetKernelArg(m_LocalSplitRadixSortKernel, 5, localBlockSize, NULL);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: local memory block");
	
	// further arguments
	clErr = clSetKernelArg(m_LocalSplitRadixSortKernel, 6, sizeof(cl_uint), (void*) &size);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: N");
	clErr = clSetKernelArg(m_LocalSplitRadixSortKernel, 7, sizeof(cl_uint), (void*) &startbit);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: startbit");
	clErr = clSetKernelArg(m_LocalSplitRadixSortKernel, 8, sizeof(cl_uint), (void*) &b);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: b");
	
	// execute kernel
	clErr = clEnqueueNDRangeKernel(CommandQueue, m_LocalSplitRadixSortKernel, 1, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
	V_RETURN_CL(clErr, "Error enqueing kernel");
}



void CRadixSort::PermutePS(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3],unsigned int size, cl_mem& inArray, cl_mem& outArray, cl_mem& zeroFlagsPS, cl_mem& oneFlagsPS, unsigned int bit) {
	
	// arguments: (__global uint* inArray, __global uint* outArray, __global uint* zeroPS, __global uint* onePS, uint N, uint bit)
	
	cl_int clErr;
	size_t globalWorkSize[1];
	size_t localWorkSize[1];
	
	// get local and global work size (they dont change)
	localWorkSize[0] = LocalWorkSize[0];
	globalWorkSize[0] = GetGlobalWorkSize(size, localWorkSize[0]);
	
	// set kernel arguments
	clErr = clSetKernelArg(m_PermutePSKernel, 0, sizeof(cl_mem), (void*) &inArray);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: inArray");
	clErr = clSetKernelArg(m_PermutePSKernel, 1, sizeof(cl_mem), (void*) &outArray);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: outArray");
	clErr = clSetKernelArg(m_PermutePSKernel, 2, sizeof(cl_mem), (void*) &zeroFlagsPS);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: zeroPS");
	clErr = clSetKernelArg(m_PermutePSKernel, 3, sizeof(cl_mem), (void*) &oneFlagsPS);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: onePS");
	clErr = clSetKernelArg(m_PermutePSKernel, 4, sizeof(cl_uint), (void*) &size);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: N");
	clErr = clSetKernelArg(m_PermutePSKernel, 5, sizeof(cl_uint), (void*) &bit);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: bit");
	
	// execute kernel
	clErr = clEnqueueNDRangeKernel(CommandQueue, m_PermutePSKernel, 1, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
	V_RETURN_CL(clErr, "Error enqueing kernel");
}


void CRadixSort::GetFlagPrefixSums(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3],unsigned int size, cl_mem& inArray, cl_mem& zeroFlagsPS, cl_mem& oneFlagsPS, unsigned int bit) {
	// get Flags for both
	GenerateFlags(Context, CommandQueue, LocalWorkSize, size, inArray, zeroFlagsPS, bit, true);
	GenerateFlags(Context, CommandQueue, LocalWorkSize, size, inArray, oneFlagsPS, bit, false);
	
	// calculate inclusive prefix sum of flags
	Scan_Naive(Context, CommandQueue, LocalWorkSize, size, zeroFlagsPS);
	Scan_Naive(Context, CommandQueue, LocalWorkSize, size, oneFlagsPS);
	
	// add maximum sum of zeroFlags to every element in oneFlags (offset)
	AddLastElement(Context, CommandQueue, LocalWorkSize, size, zeroFlagsPS, oneFlagsPS);
}

// executes a kernel that adds sum[size-1] to every element in inArray
void CRadixSort::AddLastElement(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3], unsigned int size, cl_mem& sum, cl_mem& inArray) {
	cl_int clErr;
	size_t globalWorkSize[1];
	size_t localWorkSize[1];
	
	// get local and global work size (they dont change)
	localWorkSize[0] = LocalWorkSize[0];
	globalWorkSize[0] = GetGlobalWorkSize(size, localWorkSize[0]);
	
	// set kernel arguments
	clErr = clSetKernelArg(m_AddLastElementKernel, 0, sizeof(cl_mem), (void*) &sum);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: sum");
	clErr = clSetKernelArg(m_AddLastElementKernel, 1, sizeof(cl_mem), (void*) &inArray);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: inArray");
	clErr = clSetKernelArg(m_AddLastElementKernel, 2, sizeof(cl_uint), (void*) &size);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: N");
	
	// execute kernel
	clErr = clEnqueueNDRangeKernel(CommandQueue, m_AddLastElementKernel, 1, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
	V_RETURN_CL(clErr, "Error enqueing kernel");
}

void CRadixSort::GenerateFlags(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3], unsigned int size, cl_mem& inArray, cl_mem& flags, unsigned int bit, bool invert) {
	cl_int clErr;
	size_t globalWorkSize[1];
	size_t localWorkSize[1];
	
	// get local and global work size (they dont change)
	localWorkSize[0] = LocalWorkSize[0];
	globalWorkSize[0] = GetGlobalWorkSize(size, localWorkSize[0]);
	
	// set kernel arguments
	clErr = clSetKernelArg(m_GetBitFlagsKernel, 0, sizeof(cl_mem), (void*) &inArray);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: inArray");
	clErr = clSetKernelArg(m_GetBitFlagsKernel, 1, sizeof(cl_mem), (void*) &flags);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: flags");
	// int N, uint bit, uint bitOne
	clErr = clSetKernelArg(m_GetBitFlagsKernel, 2, sizeof(cl_uint), (void*) &size);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: N");
	clErr = clSetKernelArg(m_GetBitFlagsKernel, 3, sizeof(cl_uint), (void*) &bit);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: bit");
	unsigned int bitFlagOne = (invert)?1:0;
	clErr = clSetKernelArg(m_GetBitFlagsKernel, 4, sizeof(cl_uint), (void*) &bitFlagOne);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: bitOne");
	
	// execute kernel
	clErr = clEnqueueNDRangeKernel(CommandQueue, m_GetBitFlagsKernel, 1, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
	V_RETURN_CL(clErr, "Error enqueing kernel");
}



void CRadixSort::Scan_Naive(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3], unsigned int size, cl_mem& array) {
	// define local variables
	cl_int clErr;
	size_t globalWorkSize[1];
	size_t localWorkSize[1];
	cl_mem swap_tmp;
	  
	// set kernel arguments that dont change
	clErr = clSetKernelArg(m_ScanNaiveKernel, 2, sizeof(cl_uint), (void*) &size);
	V_RETURN_CL(clErr, "Error setting Kernel Argument: N");
	
	// create pong array
	cl_mem pongArray = clCreateBuffer(Context, CL_MEM_READ_WRITE, sizeof(cl_uint) * size, NULL, &clErr);
	V_RETURN_CL(clErr, "Error allocating device array pong-array");
	
	// get local and global work size (they dont change)
	localWorkSize[0] = LocalWorkSize[0];
	globalWorkSize[0] = GetGlobalWorkSize(size, localWorkSize[0]);

	for (unsigned int offset = 1; offset < size; offset <<= 1) {
		// set kernel arguments
		clErr = clSetKernelArg(m_ScanNaiveKernel, 0, sizeof(cl_mem), (void*) &array);
		V_RETURN_CL(clErr, "Error setting Kernel Argument: PingArray");
		clErr = clSetKernelArg(m_ScanNaiveKernel, 1, sizeof(cl_mem), (void*) &pongArray);
		V_RETURN_CL(clErr, "Error setting Kernel Argument: PongArray");
		clErr = clSetKernelArg(m_ScanNaiveKernel, 3, sizeof(cl_uint), (void*) &offset);
		V_RETURN_CL(clErr, "Error setting Kernel Argument: offset");
	
		// enqueue kernel
		clErr = clEnqueueNDRangeKernel(CommandQueue, m_ScanNaiveKernel, 1, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
		V_RETURN_CL(clErr, "Error enqueing kernel");

		// switch ping pong arrays
		swap_tmp = array;
		array = pongArray;
		pongArray = swap_tmp;
	}
	
	SAFE_RELEASE_MEMOBJECT(pongArray);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define NUM_BANKS	32

void CRadixSort::Scan_WorkEfficient(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3], unsigned int size, cl_mem& dArray) {


	// Make sure that the local prefix sum works before you start experimenting with large arrays
	cl_int clErr = CL_SUCCESS;
	size_t globalWorkSize[1];
	size_t localWorkSize[1];
	
	unsigned int N = size;
	unsigned int nLevels = 1;
	while (N > 1){
		N /= 2 * LocalWorkSize[0];
		nLevels++;
	}
	
	// create level buffer
	cl_mem* dLevelArrays = new cl_mem[nLevels];
	N = size;
	cl_int clError2;
	for (unsigned int i = 0; i < nLevels; i++) {
		if (i > 0) {
			dLevelArrays[i] = clCreateBuffer(Context, CL_MEM_READ_WRITE, sizeof(cl_uint) * N, NULL, &clError2);
			clErr |= clError2;
		}
		else
			dLevelArrays[i] = dArray;
		
		N = max(N / (2 * LocalWorkSize[0]), LocalWorkSize[0]);
	}
	V_RETURN_CL(clErr, "Error allocating device arrays");
	
	
	  unsigned int numThreads = size;
	  for(unsigned int level = 0; level < nLevels-1; level++) {
		  numThreads >>= 1;
		  if (numThreads == 0) {
			  std::cout << "!!!!!!!!!!! MEGA FAIL !!!!!!!!!!!!!" << std::endl;
			  break;
		  }
		  // get local and global work size (they dont change)
		  localWorkSize[0] = LocalWorkSize[0];
		  globalWorkSize[0] = GetGlobalWorkSize(numThreads, localWorkSize[0]);
		  if (globalWorkSize[0] > numThreads) {
			  globalWorkSize[0] = numThreads;
		  }
		  if (localWorkSize[0] > numThreads) {
			  localWorkSize[0] = numThreads;
		  }

		  // set kernel arguments
		  clErr = clSetKernelArg(m_ScanWorkEfficientKernel, 0, sizeof(cl_mem), (void*) &dLevelArrays[level]);
		  V_RETURN_CL(clErr, "Error setting Kernel Argument: array");
		  clErr = clSetKernelArg(m_ScanWorkEfficientKernel, 1, sizeof(cl_mem), (void*) &dLevelArrays[level+1]);
		  V_RETURN_CL(clErr, "Error setting Kernel Argument: higherLevelArray");
		  size_t localBlockSize = sizeof(cl_uint)*localWorkSize[0]*2;
		  localBlockSize += (localBlockSize/NUM_BANKS);
		  clErr = clSetKernelArg(m_ScanWorkEfficientKernel, 2, localBlockSize, NULL);
		  V_RETURN_CL(clErr, "Error setting Kernel Argument: local memory block");
	  
		  // enqueue kernel
		  clErr = clEnqueueNDRangeKernel(CommandQueue, m_ScanWorkEfficientKernel, 1, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
		  V_RETURN_CL(clErr, "Error enqueing kernel");

		  if (level < nLevels-2)
			  numThreads /= localWorkSize[0];
	  }
	  // backwards adding groups up
	  for(int level = nLevels-3; level >= 0; level--) {
		  // get local and global work size (they dont change)
		  localWorkSize[0] = LocalWorkSize[0];
		  numThreads *= (2*localWorkSize[0]);
		  globalWorkSize[0] = GetGlobalWorkSize(numThreads, localWorkSize[0]);
		  if (globalWorkSize[0] > numThreads) {
			  globalWorkSize[0] = numThreads;
		  }
		  if (localWorkSize[0] > numThreads) {
			  localWorkSize[0] = numThreads;
		  }


		// set kernel arguments
		clErr = clSetKernelArg(m_ScanWorkEfficientAddKernel, 0, sizeof(cl_mem), (void*) &dLevelArrays[level+1]);
		V_RETURN_CL(clErr, "Error setting Kernel Argument: array");
		clErr = clSetKernelArg(m_ScanWorkEfficientAddKernel, 1, sizeof(cl_mem), (void*) &dLevelArrays[level]);
		V_RETURN_CL(clErr, "Error setting Kernel Argument: higherLevelArray");
		size_t localBlockSize = sizeof(cl_uint)*localWorkSize[0]*2;
		localBlockSize += (localBlockSize/NUM_BANKS);
		clErr = clSetKernelArg(m_ScanWorkEfficientAddKernel, 2, localBlockSize, NULL);
		V_RETURN_CL(clErr, "Error setting Kernel Argument: local memory block");
	
		// enqueue kernel
		clErr = clEnqueueNDRangeKernel(CommandQueue, m_ScanWorkEfficientAddKernel, 1, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
		V_RETURN_CL(clErr, "Error enqueing kernel");

	}
	
	
	for (unsigned int i = 1; i < nLevels; i++) {
		SAFE_RELEASE_MEMOBJECT(dLevelArrays[i]);
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
